var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less([
    	'bootstrap.less',
    	'template.less'
    ]);

    mix.sass('app.scss');

    mix.scripts([
    	'app.js',
    	'team.js',
    	'post.js',
    	'group.js',
    	'playoff.js',
    	'event.js',
    	'scoreboard.js',
    	'settings.js'
    ], 'public/js/app.js');
});
