app.controller('postsController', function($scope, $http) {
 
    $scope.posts = [];
    $scope.lastpage = 1;
    $scope.load_more = true;
    $scope.loading = false;
    $scope.more_loading = false;

    $scope.tournament_id;

    $scope.init = function(tournament_id) {
        $scope.tournament_id = tournament_id;
        $scope.loading = true;
        $scope.lastpage = true;
        $http({
            url: '/api/posts',
            method: "GET",
            params: {
                page:  $scope.lastpage,
                tournament_id: tournament_id
            }
        }).success(function(data, status, headers, config) {
            $scope.posts = data.data;
            if(data.current_page == data.last_page || data.total == 0)
                $scope.load_more = false;
            $scope.currentpage = data.current_page;
            $scope.loading = false;
        });
    };

    $scope.loadMore = function() {
        $scope.lastpage +=1;
        $scope.more_loading = true;
        $http({
            url: '/api/posts',
            method: "GET",
            params: {
                page:  $scope.lastpage,
                tournament_id: $scope.tournament_id
            }
        }).success(function (data, status, headers, config) {
            $scope.posts = $scope.posts.concat(data.data);
            if(data.current_page == data.last_page)
            	$scope.load_more = false;

            $scope.more_loading = false;
        });
    };

    $scope.deletePost = function(index){
    	var post = $scope.posts[index];
    	$http.delete('/api/posts/'+post.id)
    	.success(function(){
    		$scope.posts.splice(index,1);
    	});
    }

    $scope.imageStrings = [];
    $scope.processFiles = function(files){
      angular.forEach(files, function(flowFile, i){
         var fileReader = new FileReader();
            fileReader.onload = function (event) {
              var uri = event.target.result;
                $scope.imageStrings[i] = uri;  
                $scope.$apply();  
            };
            fileReader.readAsDataURL(flowFile.file);
      });

    };

    $scope.clickUploadImage = function(){
        angular.element('#uploadImage').trigger('click');
    };

});
 