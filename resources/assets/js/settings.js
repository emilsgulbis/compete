app.controller('settingsController', function($scope, $http, $window) {

    $scope.tournament = {};
    $scope.loading_slug = false;
    $scope.slug_valid = false;

    $scope.tournament_id;

    $scope.init = function(tournament_id) {
        $scope.tournament_id = tournament_id;
        $scope.getSlug(tournament_id);
    }

    $scope.getSlug = function(id){
        $scope.loading_slug = true;
        $http.get('/api/tournament/slug',{
            params: {
                id : id
            }
        }).
        success(function(data, status, headers, config) {
            $scope.tournament.slug = data;
            $scope.tournament.start_slug = data;
            $scope.loading_slug = false;
            $scope.slug_valid = true;
        });
    }

    $scope.validateSlug = function(){
        $scope.loading_slug = true;

        if($scope.tournament.slug != $scope.tournament.start_slug){
            $http.get('/api/tournament/slug/validate', {
                params:{
                    slug : $scope.tournament.slug
                }
            }).
            success(function(data, status, headers, config) {
                $scope.slug_valid = data;
            });
        }
        else{
            $scope.slug_valid = true;
        }
        $scope.loading_slug = false;
    }

    $scope.clickUploadLogo = function(){
        angular.element('#uploadLogo').trigger('click');
    };

    $scope.clickUploadPoster = function(){
        angular.element('#uploadPoster').trigger('click');
    };

    $scope.deleteTournament = function(slug){
    	swal({
    		title: "Tiešām vēlies dzēst?",
    		text: "Pēc dzēšanas savu turnīru atgūt nebūs iespējams!",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: "#DD6B55",
    		confirmButtonText: "Jā!",
    		cancelButtonText: "Nē!",
    		closeOnConfirm: false,
    		closeOnCancel: false
    	}, function(isConfirm){
    		if (isConfirm) {
    			$window.location.href = '/t/'+slug+'/remove';
    		} else {
    			swal("Dzēšana atcelta!", "Turnīrs netika izdzēsts.", "error");
    		}
    	});
    }

});