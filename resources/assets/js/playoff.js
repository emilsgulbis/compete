app.controller('playoffController', function($scope, $http, CSRF_TOKEN) {

	function needKey(arraytosearch, key, valuetosearch) {
		for (var i = 0; i < arraytosearch.length; i++) {
			if (arraytosearch[i][key] == valuetosearch) {
				return false;
			}
		}
		return true;
	}
	
	$scope.tournament_id;

	$scope.playoff = {};

	$scope.teams = [];
	$scope.playoffs = [];
	$scope.games = [];

	$scope.need = needKey;

	$scope.loading = false;
	$scope.loading_remove = false;
	
	$scope.init = function(tournament_id) {
		$scope.tournament_id = tournament_id;
		$scope.loading = false;

		$http.get('/api/playoff', {
			params: {
				tournament_id: tournament_id
			}
		}).
		success(function(data, status, headers, config) {
			$scope.playoffs = data;
		});

		$http.get('/api/group/participants', {
			params:{
				tournament_id: $scope.tournament_id
			}
		}).
		success(function(data, status, headers, config) {
			$scope.teams = data;
			$scope.loading = false;
		});
	}

	$scope.addPlayoff = function() {
		$scope.loading = true;
		if($scope.playoff.name != ''){
			$http.post('/api/playoff', {
				name: $scope.playoff.name,
				tournament_id: $scope.tournament_id,
				level : $scope.playoff.level,
				_token : CSRF_TOKEN
			}).success(function(data, status, headers, config) {
				$scope.playoffs = data;
				$scope.playoff = {};
				$scope.loading = false;
			});
		}
	};

	$scope.removePlayoffAlert = function(index){
		swal({
    		title: "Tiešām vēlies dzēst?",
    		text: "Šī darbība ir neatgriezeniska!",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: "#DD6B55",
    		confirmButtonText: "Jā!",
    		cancelButtonText: "Nē!",
    		closeOnConfirm: false,
    		closeOnCancel: false
    	}, function(isConfirm){
    		if (isConfirm) {
    			swal("Dzēšam...");
    			$scope.removePlayoff(index);
    		} else {
    			swal("Dzēšana atcelta!", "Izslēgšanas spēļu koks netika izdzēsts.", "error");
    		}
    	});
	}

	$scope.removePlayoff = function(index){
		var playoff = $scope.playoffs[index];
		$scope.loading_remove = true;


		$http.post('/api/playoff/remove', {
			id: playoff.id,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config) {
			$scope.loading_remove = false;
			$scope.playoffs.splice(index, 1);
			swal({
				title: "Dzēsts!",
				text: "Izslēgšanas spēļu koks veiksmīgi izdzēsts!",
				type: "success",
				timer: 1000
			});
		});
	}

	$scope.editGame = function(games){
		$scope.games = games;
	}

	$scope.updatePlayoff = function(data, playoff_id){
		return $http.post('/api/playoff/update',{
			id: playoff_id,
			name: data,
			_token : CSRF_TOKEN
		});
	}

	$scope.saveGame = function(){
		$scope.loading = true;
		$http.post('/api/playoff/game', {
			games: $scope.games,
			tournament_id: $scope.tournament_id,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config) {
			$scope.games = [];
			$scope.playoffs = data;
			$scope.loading = false;
		});
	} 
});

$(function(){
	$('.app').on({
        mouseenter: function(e) {
            $('.bracket-team-' + $(this).data('team'))
                .addClass('bracket-team-hover');
        },
        mouseleave: function(e) {
            $('.bracket-team-' + $(this).data('team'))
                .removeClass('bracket-team-hover');
        }
    }, '.bracket-team');
});