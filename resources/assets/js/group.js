app.controller('groupController', function($scope, $http, CSRF_TOKEN) {

	function needKey(arraytosearch, key, valuetosearch) {
		for (var i = 0; i < arraytosearch.length; i++) {
			if (arraytosearch[i][key] == valuetosearch) {
				return false;
			}
		}
		return true;
	}

	$scope.group = {};
	$scope.team = {};
 
	$scope.tournament_id;
	$scope.match;

	$scope.teams = [];
	$scope.groups = [];
	
	$scope.need = needKey;

	$scope.loading = false;
	$scope.loading_remove = false;
	
	$scope.init = function(tournament_id) {
		$scope.tournament_id = tournament_id;
		$scope.loading = true;
		$http.get('/api/group', {
			params: {
				tournament_id: tournament_id
			}
		}).
		success(function(data, status, headers, config) {
			$scope.groups = data;
		});
		$http.get('/api/group/participants', {
			params:{
				tournament_id: $scope.tournament_id
			}
		}).
		success(function(data, status, headers, config) {
			$scope.teams = data;
			$scope.loading = false;
		});
	}
 
	$scope.addGroup = function() {
		$scope.loading = true;
		if($scope.group.name != ''){
			$http.post('/api/group', {
				name: $scope.group.name,
				tournament_id: $scope.tournament_id,
				_token : CSRF_TOKEN
			}).success(function(data, status, headers, config) {
				$scope.groups.push(data);
				$scope.group = {};
				$scope.loading = false;
			});
		}
	};

	$scope.addParticipant = function(){
		$http.post('/api/group/participant', {
			team_id: $scope.team.id,
			group_id: $scope.group.id,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config) {
			$scope.groups = data;
		});
		$scope.team = {};
		$scope.group = {};
	}

	$scope.updateGroup = function(data, group_id){
		return $http.post('/api/group/update',{
			id: group_id,
			name: data,
			_token : CSRF_TOKEN
		});
	}

	$scope.removeGroup = function(index){
		var group = $scope.groups[index];
		$scope.loading_remove = true;

		$http.post('/api/group/remove', {
			id: group.id,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config) {
			$scope.loading_remove = false;
			$scope.groups.splice(index, 1);
			swal({
				title: "Dzēsts!",
				text: "Grupa veiksmīgi izdzēsta!",
				type: "success",
				timer: 1000
			});
		});
	}

	$scope.removeGroupAlert = function(index){
		swal({
    		title: "Tiešām vēlies dzēst?",
    		text: "Šī darbība ir neatgriezeniska!",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: "#DD6B55",
    		confirmButtonText: "Jā!",
    		cancelButtonText: "Nē!",
    		closeOnConfirm: false,
    		closeOnCancel: false
    	}, function(isConfirm){
    		if (isConfirm) {
    			swal("Dzēšam...");
    			$scope.removeGroup(index);
    		} else {
    			swal("Dzēšana atcelta!", "Grupa netika izdzēsta.", "error");
    		}
    	});
	}

	$scope.getMatch = function(match_id){
		$http.get('/api/match',{
			params: {
				id : match_id
			}
		}).
		success(function(data, status, headers, config) {
			$scope.match = data;
		});
	}

	$scope.saveMatch = function(){
		$http.post('/api/match/save', {
			match: $scope.match,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config){
			$scope.match = '';
			$scope.groups = data;
		});
	}

	$scope.createMatch = function(home_id, away_id){
		$http.post('/api/match/create', {
			home_id : home_id,
			away_id : away_id,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config){
			$scope.match = data;
		});
	}

	$scope.deleteMatch = function(){
		$http.post('/api/match/delete', {
			match: $scope.match,
			_token : CSRF_TOKEN
		}).success(function(data, status, headers, config){
			$scope.match = '';
			$scope.groups = data;
		});
	}
 
});
 