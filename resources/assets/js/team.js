app.controller('teamController', function($scope, $http) {
 
	$scope.teams = [];
	$scope.loading = false;
	$scope.loading_create = false;
	$scope.tournament_id;
	$scope.team = '';
 
	$scope.init = function(tournament_id) {
		$scope.tournament_id = tournament_id;
		$scope.loading = true;
		$http.get('/api/teams/'+tournament_id).
		success(function(data, status, headers, config) {
			$scope.teams = data;
			$scope.loading = false;
		});
	}
 
	$scope.addTeam = function() {
		$scope.loading_create = true;
		if($scope.team != ''){
			$http.post('/api/teams', {
				name: $scope.team.name,
				tournament_id: $scope.tournament_id
			}).success(function(data, status, headers, config) {
				$scope.teams.push(data);
				$scope.team = '';
				$scope.loading_create = false;
			});
		}
	};
 
	$scope.removeTeam = function(index) {
		$scope.loading = true;
 
		var team = $scope.teams[index];
 
		$http.post('/api/teams/remove', {
			team_id: team.id,
			tournament_id: $scope.tournament_id
		}).success(function(data, status, headers, config) {
			$scope.teams.splice(index, 1);
			$scope.loading = false;
			swal({
				title: "Dzēsts!",
				text: "Dalībnieks veiksmīgi izdzēsts!",
				type: "success",
				timer: 1000
			});
		});
	};

	$scope.removeTeamAlert = function(index){
		swal({
    		title: "Tiešām vēlies dzēst?",
    		text: "Šī darbība ir neatgriezeniska!",
    		type: "warning",
    		showCancelButton: true,
    		confirmButtonColor: "#DD6B55",
    		confirmButtonText: "Jā!",
    		cancelButtonText: "Nē!",
    		closeOnConfirm: false,
    		closeOnCancel: false
    	}, function(isConfirm){
    		if (isConfirm) {
    			swal("Dzēšam...");
    			$scope.removeTeam(index);
    		} else {
    			swal("Dzēšana atcelta!", "Dalībnieks netika izdzēsts.", "error");
    		}
    	});
	}
 
 
});
 