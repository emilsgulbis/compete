app.controller('scoreboardController', function($scope, $http, CSRF_TOKEN) {

    var socket = io.connect('http://188.166.125.57:3000');

    $scope.event_id;
    $scope.message = '';
    $scope.event = {};

    $scope.labels = [];
    $scope.series = [];
    $scope.data = [];

    $scope.init = function(event_id) {
        $scope.event_id = event_id;
        socket.emit('start watch', event_id);
    }

    $scope.removeHistory = function(id){
        $http.post('/api/event/history-remove', {
            id: id,
            _token : CSRF_TOKEN
        }).success(function(data, status, headers, config) {

            socket.emit('remove item', data);
            console.log(data)

            swal({
                title: "Dzēsts!",
                text: "Notikums veiksmīgi izdzēsts!",
                type: "success",
                timer: 1000
            });
        });
    }

    $scope.removeHistoryAlert = function(index){
        swal({
            title: "Tiešām vēlies dzēst?",
            text: "Šī darbība ir neatgriezeniska!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Jā!",
            cancelButtonText: "Nē!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                swal("Dzēšam...");
                $scope.removeHistory(index);
            } else {
                swal("Dzēšana atcelta!", "Notikums netika izdzēsts.", "error");
            }
        });
    }

    $scope.updateScore = function(key, value){
        socket.emit('change score', {
            key : key, 
            value : value,
            message : $scope.message,
            event_id: $scope.event_id,
            _token : CSRF_TOKEN
        });
    }

    $scope.addMessage = function(){
        socket.emit('change score', {
            key : 'msg',
            message : $scope.message,
            event_id: $scope.event_id,
            _token : CSRF_TOKEN
        });
    }
    
    socket.on('push score', function(data){
        $scope.event = data.event;
        $scope.statistic = data.statistic;

        $scope.labels = data.statistic.dates;
        $scope.series = [data.event.home_team.name, data.event.away_team.name];
        $scope.data = [data.statistic.home, data.statistic.away];

        $scope.message = '';
        $scope.$apply();
    });

});