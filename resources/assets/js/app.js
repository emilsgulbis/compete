var app = angular.module('competeApp', ["xeditable", "ui.bootstrap", "chart.js", "ngFileUpload"], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
});

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});

app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});

app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
		colours: ['#23b7e5', '#7266ba'],
		
    });
    // Configure all line charts
    ChartJsProvider.setOptions('Line', {
		datasetFill: false
    });
}]);
