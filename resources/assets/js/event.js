app.controller('eventController', function($scope, $http, CSRF_TOKEN) {
 
    $scope.events = [];
    $scope.loading = false;
    $scope.teams = [];
    $scope.event = {};

    $scope.tournament_id;

    $scope.init = function(tournament_id) {
        $scope.tournament_id = tournament_id;
        $scope.event.tournament_id = $scope.tournament_id;
        $scope.loading = true;

        $http.get('/api/event', {
            params: {tournament_id: tournament_id}
        }).
        success(function(data, status, headers, config) {
            $scope.events = data;
            $scope.loading = false;
        });

        $http.get('/api/group/participants', {
            params:{tournament_id: $scope.tournament_id}
        }).
        success(function(data, status, headers, config) {
            $scope.teams = data;
            $scope.loading = false;
        });
        
    };

    $scope.addEvent = function(){
        $scope.loading = true;
    
        $http.post('/api/event', {
            event: $scope.event,
            tournament_id: $scope.tournament_id,
            _token : CSRF_TOKEN
        }).success(function(data, status, headers, config) {
            $scope.event = {tournament_id: $scope.tournament_id};
            $scope.events = data;
            $scope.today();
            $scope.loading = false;
        });
    }

    $scope.removeEvent= function(parent_index, index) {
        $scope.loading = true;
        var ev = $scope.events[parent_index].games[index];

        $http.post('/api/event/remove', {
            event_id: ev.id,
            _token : CSRF_TOKEN
        }).success(function(data, status, headers, config) {
            $scope.events[parent_index].games.splice(index, 1);
            if($scope.events[parent_index].games.length == 0)
                $scope.events.splice(parent_index,1);
            $scope.loading = false;
        });
    };

    // Datepicker
    $scope.dateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.today = function() {
        $scope.event.date = new Date();
    };
    $scope.today();

    $scope.dateOptions = {
        formatYear: 'yyyy',
        showWeeks: false,
        startingDay: 1,
        class: 'datepicker'
    };
});