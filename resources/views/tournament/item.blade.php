<div class="item">
	<div class="pos-rlt">
		@if($tournament->sport_id)
			<div class="top m-l-sm m-t-sm">
				<span class="label bg-info">{{$tournament->sport->name}}</span>
			</div>
		@endif
		<a href="{{url('t', $tournament->slug)}}">
			@if($tournament->logo)
				{!! Html::image($tournament->logo_url, $tournament->name, ['class'=>'img-full r r-2x']) !!}
			@else
				{!! Html::image('img/logo-placeholder.jpg', $tournament->name, ['class'=>'img-full r r-2x']) !!}
			@endif
		</a>
	</div>
	<div class="padder-v">
	<a class="text-ellipsis" href="{{url('t', $tournament->slug)}}">{{$tournament->name}}</a>
	@if(!$tournament->visible)
	<span class="text-ellipsis text-xs text-muted">Neredzams</span>
	@endif
	</div>
</div>