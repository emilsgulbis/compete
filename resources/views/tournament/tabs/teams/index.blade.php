@extends('tournament.single')

@section('tabs')


<div ng-controller="teamController" ng-init="init({{$tournament->id}})">
	
	@if($tournament->owner())
	<form class="m-b" name="participantform">
		<h4>Izveidot jaunu {{$tournament->playerType() ? 'spēlētāju' : 'komandu'}}</h4>
		<div class="input-group">
			<input type="text" class="form-control" placeholder="{{ ($tournament->playerType() ? 'Vārds Uzvārds' : 'Komandas nosaukums') }} (obligāts)" ng-model="team.name" required>
			<span class="input-group-btn">
				<button class="btn btn-success" ng-click="addTeam()" ng-disabled="participantform.$invalid">
					<span ng-show="!loading_create">Pievienot</span>
					<i ng-show="loading_create" class="fa fa-spin fa-circle-o-notch"></i>
				</button>
			</span>
		</div>
	</form>
	@endif

	<div class="row">
		<div class="col-sm-6 col-md-4 col-lg-3" ng-repeat='team in teams'>
			<a class="panel hbox hover-anchor pos-rlt wrapper" href="#">
				{{-- <div class="col v-middle thumb">
					<img src="{{asset('img/b3.jpg')}}" alt="">
				</div>
				<div class="col v-middle p-l p-r w-full">
					<h3 class="h5"><% team.name %></h3>
					<span class="text-muted text-xs" ng-if="team.place">
						<% team.place %>
					</span>
				</div> --}}
				<h3 class="h5"><% team.name %></h3>
				@if($tournament->owner())
					<span ng-click="removeTeamAlert($index)" class="right-xs top-n pos-abt text-muted ">
						<i class="fa fa-times"></i>
					</span>
				@endif
			</a>
		</div>
	</div>
</div>


@endsection