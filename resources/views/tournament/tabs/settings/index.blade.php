@extends('tournament.single')

@section('tabs')

@if($tournament->owner())
    <div class="alert alert-info">
        <p>Turnīrs ir redzams <strong>{{($tournament->visible ? "visiem" : "tikai Tev")}}!</strong></p>
    </div>
@endif

{!! Form::open(array('route'=>'save-settings','files'=>true)) !!}

<h4 class="font-bold">Profila bildes</h4>

<a href="#" class="btn btn-default btn-addon" ng-click="clickUploadLogo()"><i class="icon icon-camera"></i> Pievienot bildi</a>
<a href="#" class="btn btn-default btn-addon" ng-click="clickUploadPoster()"><i class="icon icon-picture"></i> Mainīt fona bildi</a>

<input type="file" ngf-select="" ng-model="logo" name="logo" accept="image/*" class="hidden" id="uploadLogo">
<input type="file" ngf-select="" ng-model="poster" name="poster" accept="image/*" class="hidden" id="uploadPoster">

<div class="line line-dashed b-b line-lg b-gray"></div>

<h4 class="font-bold">Galvenā informācija</h4>

{!! Form::hidden('id', $tournament->id) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	{!! Form::label('name', 'Nosaukums', ['class'=>'control-label']) !!}
	{!! Form::text('name', old('name', $tournament->name), ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('slug', 'Adrese') !!}
	<div class="input-group">
		<span class="input-group-addon">http://compete.lv/t/</span>
		{!! Form::text('slug', '' ,['class'=>'form-control', 'ng-keyup'=>'validateSlug($data)', 'ng-model'=>'tournament.slug', 'autocomplete'=>'off']) !!}
		<span class="input-group-addon" ng-if="!loading_slug">
			<i class="fa fa-check text-success" ng-if="slug_valid"></i>
			<i class="fa fa-times text-danger" ng-if="!slug_valid"></i>
		</span>
		<span class="input-group-addon" ng-if="loading_slug">
			<i class="fa fa-spin fa-circle-o-notch"></i>
		</span>
	</div>
</div>

<div class="form-group">
	{!! Form::label('description', 'Apraksts') !!}
	{!! Form::textarea('description', $tournament->description, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('visible', 'Redzams') !!}
	{!! Form::select('visible', ['Tikai man', 'Visiem'], $tournament->visible, ['class'=>'form-control'] )!!}
</div>

<div class="line line-dashed b-b line-lg b-gray"></div>

<h4 class="font-bold">Turnīrs</h4>
	
<div class="form-group">
	{!! Form::label('sport_id', 'Sporta veids') !!}
	{!! Form::select('sport_id', \App\Sport::lists('name', 'id') + [''=>'Cits'], $tournament->sport_id, ['class'=>'form-control'] )!!}
</div>

<div class="form-group">
	{!! Form::label('type', 'Turnīra veids') !!}
	{!! Form::select('type', ['Individuāls turnīrs', 'Komandu turnīrs'], $tournament->type, ['class'=>'form-control'] )!!}
</div>

<div class="form-group">
	<label class="m-b-n">Izspēles kārtība</label>
	<div class="checkbox">
		<label class="i-checks">
			{!! Form::checkbox('groups', 1, $tournament->groups) !!}
			<i></i>
			Grupu turnīrs
		</label>
    </div>

    <div class="checkbox">
		<label class="i-checks">
			{!! Form::checkbox('playoff', 1, $tournament->playoff) !!}
			<i></i>
			Izslēgšanas spēles
		</label>
    </div>
</div>

<div class="line line-dashed b-b line-lg b-gray"></div>

<div class="form-group">
	{!! Form::submit('Saglabāt', ['class'=>'btn btn-success'])!!}
	<a ng-click="deleteTournament('{{$tournament->slug}}')" class="btn btn-danger">Dzēst turnīru</a>
</div>

{!! Form::close() !!}

@endsection