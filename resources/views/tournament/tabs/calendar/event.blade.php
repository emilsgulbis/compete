@extends('tournament.single')

@section('tabs')

@if($event)

    @if($event->live)
    <div ng-controller="scoreboardController" ng-init="init({{$event->id}})">

        <div class="row">
            <div class="col-md-6">
                <!-- Score table -->
                <h4>Rezultāts</h4>
                
                @if($tournament->owner())
                    <div class="input-group">
                        <input type="text" class="form-control no-radius input-sm" ng-model="message" placeholder="Ziņa">
                        <span class="input-group-btn">
                            <button ng-click="addMessage()" class="btn btn-sm btn-default">Pievienot</button>
                        </span>
                    </div>
                    
                @endif
                
                <div class="row no-gutter text-center m-t-sm m-b-lg">
                    <div class="col-xs-6 bg-info">
                        <div class="wrapper">
                            <span class="m-b-xs h1 block text-white"><% event.home_score %></span>
                            <span class="h4"><% event.home_team.name %></span>
                        </div>
                        @if($tournament->owner())
                        <button type="button" class="btn btn-info dker no-borders no-radius w-full" ng-click="updateScore('home_score', 1)">+1</button>
                        @endif
                    </div>
                    <div class="col-xs-6 bg-primary">
                        <div class="wrapper">
                           <span class="m-b-xs h1 block text-white"><% event.away_score %></span>
                            <span class="h4"><% event.away_team.name %></span>
                        </div>
                        @if($tournament->owner())
                        <button type="button" class="btn btn-primary dker no-borders no-radius w-full" ng-click="updateScore('away_score', 1)">+1</button>
                        @endif
                    </div>
                </div>

                <h4>Izmaiņu grafiks</h4>     
                <canvas id="line" height="300" class="chart chart-line" data="data" labels="labels" series="series" click="onClick"></canvas>   

                @if($tournament->owner())
                <h4>Tiešraides režīms</h4>
                {!!Form::open(array('route'=>'change-live'))!!}
                    {!!Form::hidden('event_id', $event->id) !!}
                    {!!Form::hidden('live', 0) !!}
                    {!!Form::submit('Izslēgt', ['class'=>'btn btn-success'])!!}
                {!!Form::close()!!}
                @endif 
            </div>

            <div class="col-md-6">
                <!-- List -->
                <h4>Notikumi</h4>
                <ul class="list-group list-group-sp">
                    @if($tournament->owner())
                    <li class="list-group-item pos-rlt" ng-class="{'b-l-3x b-l-info' : item.key == 'home_score', 'b-l-3x b-l-primary' : item.key == 'away_score'}" ng-repeat="item in event.history | orderBy:'-id' ">
                        <strong ng-if="item.value">+<% item.value %></strong>
                        <span ng-if="item.message"><% item.message %></span>
                        <a ng-click="removeHistoryAlert(item.id)" class="fa fa-times right-xs m-r-xs pos-abt text-muted"></a>
                    </li>
                    @else
                    <li class="list-group-item" ng-class="{'b-l-3x b-l-info' : item.key == 'home_score', 'b-l-3x b-l-primary' : item.key == 'away_score'}" ng-repeat="item in event.public_history |orderBy:'-id' ">
                        <strong ng-if="item.value">+<% item.value %></strong>
                        <span ng-if="item.message"><% item.message %></span>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @else
    
    @if($tournament->owner())
    <div class="row">
        <div class="col-md-6">
            <h4>Spēle</h4>
            <div class="row no-gutter text-center m-t-sm m-b-lg">
                <div class="col-xs-6 bg-info">
                    <div class="wrapper">
                        <span class="h4">{{$event->home_team->name}}</span>
                    </div>
                </div>
                <div class="col-xs-6 bg-primary">
                    <div class="wrapper">
                        <span class="h4">{{$event->away_team->name}}</span>
                    </div>
                </div>
            </div>

            <h4>Tiešraides režīms</h4>
            {!!Form::open(array('route'=>'change-live'))!!}
                {!!Form::hidden('event_id', $event->id) !!}
                {!!Form::hidden('live', 1) !!}
                {!!Form::submit('Ieslēgt', ['class'=>'btn btn-success'])!!}
            {!!Form::close()!!}
        </div>
    </div>
    @else
        <div class="alert alert-danger">
            Šis notikums nav pieejams tiešraides režīmā
        </div>
    @endif

    @endif

@else
    <div class="alert alert-info">
        Nav šāda notikuma!
    </div>
@endif

@endsection

@section ('scripts')
	<script src="http://188.166.125.57:3000/socket.io/socket.io.js"></script>
	<script>
        
        
    </script>
@endsection