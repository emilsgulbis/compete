@extends('tournament.single')

@section('tabs')

<div ng-controller="eventController" ng-init="init({{$tournament->id}})">
    
    <div class="text-center m-t-lg" ng-if="loading">
        <i class="fa fa-3x fa-spin fa-circle-o-notch"></i>
    </div>

    <div ng-if="!loading">
        @if($tournament->owner())
        <form class="m-b" name="eventform">
            <h5>Jauns notikums</h5>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-team-1">1. {{$tournament->playerType() ? 'spēlētājs' : 'komanda'}} <span ng-show="eventform.home_team_id.$error.required" class="text-danger">*</span></label>
                        <select name="home_team_id" class="form-control" ng-model="event.home_team_id" ng-options="team.id as team.name for team in teams" ng-required="event.away_team_id">
                            <option value="" disabled></option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-team-2">2. {{$tournament->playerType() ? 'spēlētājs' : 'komanda'}} <span ng-show="eventform.away_team_id.$error.required" class="text-danger">*</span></label>
                        <select name="away_team_id" class="form-control" ng-model="event.away_team_id" ng-options="team.id as team.name for team in teams" ng-required="event.home_team_id">
                            <option value="" disabled></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-name">Virsraksts <span ng-show="eventform.name.$error.required" class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" id="event-name" ng-model="event.name" ng-required="!event.home_team_id && !event.away_team_id">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-date">Datums<span class="text-danger">*</span> <em>(dd.mm.gggg)</em></label>
                        <div class="input-group" ng-click="dateOpen($event)">
                            <input type="text" class="form-control" id="event-date" ng-model="event.date" datepicker-popup="dd.MM.yyyy" is-open="opened" datepicker-options="dateOptions" ng-required="true" show-button-bar="false">
                            <span class="input-group-addon">
                                <span class="icon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-datetime">Laiks <em>()</em></label>
                        <input type="text" class="form-control" id="event-time" placeholder="12:00" ng-model="event.time">  
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="event-place">Norises vieta</label>
                        <input type="text" id="event-place" class="form-control" ng-model="event.place">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <p ng-show="eventform.$invalid"><span class="text-danger">*</span><em>Obligāti aizpildāmie lauki</em></p>
                <button class="btn btn-success" ng-click="addEvent()" ng-disabled="eventform.$invalid">Pievienot</button>
            </div>
            <div class="line line-dashed b-b line-lg b-gray"></div>
        </form>
        @endif

        <div class="panel" ng-repeat="event in events">
            <div class="panel-heading no-border bg-primary dker">
                <% event.date | date:'dd.MM.yyyy'%>
            </div>
            
            <div class="panel-body">
                <ul class="list-group list-group-sp list-group-alt m-b-none">
                    <li class="list-group-item" ng-repeat="game in event.games">
                        @if($tournament->owner())
                            <span ng-if="game.live == 1" class="label bg-danger pos-abt right m-r">TIEŠRAIDE</span>
                            <a ng-click="removeEvent($parent.$index, $index)" class="right-xs top-n pos-abt text-muted ">
                                <i class="fa fa-times"></i>
                            </a>
                        @else
                         <span ng-if="game.live == 1" class="label bg-danger pos-abt right">TIEŠRAIDE</span>
                        @endif
                        
                        <span ng-if="game.name" class="block"><%game.name%></span>
                        @if($tournament->owner())
                            <a href="{{url('t/'.$tournament->slug.'/calendar')}}/<%game.id%>" class="text-md" ng-if="game.home_team"><%game.home_team.name%> - <%game.away_team.name %></a>
                        @else
                            <a ng-if="game.live == 1" href="{{url('t/'.$tournament->slug.'/calendar')}}/<%game.id%>" class="text-md" ng-if="game.home_team"><%game.home_team.name%> - <%game.away_team.name %></a>
                            <span ng-if="game.live != 1" class="text-md" ng-if="game.home_team"><%game.home_team.name%> - <%game.away_team.name %></span>
                        @endif
                       
                        <div class="text-muted" ng-if="game.time || game.place">
                            <div class="line b-b line-lg m-b-sm m-t-sm"></div>
                            <i class="fa fa-clock-o" ng-if="game.time"></i>
                            <span class="m-r-sm" ng-if="game.time"><%game.time %></span>
                            <i class="fa fa-map-marker" ng-if="game.place"></i>
                            <span ng-if="game.place"><%game.place%></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection