@extends('tournament.single')

@section('tabs')

<div ng-controller="groupController" ng-init="init({{$tournament->id}})">

	<div class="text-center m-t-lg" ng-if="loading">
		<i class="fa fa-3x fa-spin fa-circle-o-notch"></i>
	</div>
	
	<div ng-if="!loading">
		@if($tournament->owner())
		<form class="m-b" name="groupform">
			<h4>Izveidot jaunu grupu</h4>
			<div class="form-group">
				<div class="input-group" name="nameform">
					<input type="text" class="form-control" placeholder="Grupas nosaukums (obligāts)" ng-model="group.name" name="name" required>
					<span class="input-group-btn">
						<button class="btn btn-success" ng-click="addGroup()" ng-disabled="groupform.name.$invalid">Pievienot</button>
					</span>
				</div>
			</div>
			<div class="line line-dashed b-b line-lg b-gray"></div>
			<h4>Pievienot komandu grupai</h4>
			<div class="form-group">
				<label for="group-team">{{ ($tournament->playerType() ? 'Spēlētājs' : 'Komanda') }} <span class="text-danger">*</span></label>
				<select class="form-control" ng-model="team.id" name="team" required ng-options="team.id as team.name for team in teams" id="group-team">
					<option value=""></option>
				</select>
			</div>
			<div class="form-group">
				<label for="group-groupw">Grupa <span class="text-danger">*</span></label>
				<select class="form-control" ng-model="group.id" id="group-group" name="group" required ng-options="group.id as group.name for group in groups">
					<option value=""></option>
				</select>
			</div>
			<p><span class="text-danger">*</span> <em>Obligāti aizpildāmie lauki</em></p>
			<div class="form-group">
				<button class="pull-right btn btn-primary" ng-click="addParticipant()" ng-disabled="groupform.team.$invalid || groupform.group.$invalid">Pievienot</button>
				<div class="clearfix"></div>
			</div>
			<div class="line line-dashed b-b line-lg b-gray"></div>
		</form>
		@endif

		<div class="panel panel-default" ng-repeat="group in groups">
			<div class="panel-heading text-center pos-rlt">
				@if($tournament->owner())
					<a ng-click="removeGroupAlert($index)" class="right-xs m-r-xs pos-abt text-muted">
						<i ng-show="!loading_remove" class="fa fa-times"></i>
						<i ng-show="loading_remove" class="fa fa-spin fa-circle-o-notch"></i>
					</a>
	            	<a editable-text="group.name" onbeforesave="updateGroup($data, group.id)"><% group.name || 'ievadiet nosaukumu' %></a>
	            @else
					<% group.name %>
	            @endif
			</div>

			<i class="fa fa-spin fa-refresh" ng-if="loading"></i>
			 
			<div class="table-responsive" id="table-matches">
				<table class="table table-bordered b-t b-light" ng-if="group.group_participants.length">
					<tr>
						<td></td>
						<td ng-repeat="team in group.group_participants | orderBy:['-points', '-plus']" class="text-center"><% team.name | uppercase |limitTo:2 %></td>
						<td class="text-center">Punkti</td>
						<td class="text-center">+/-</td>
						<td class="text-center">Vieta</td>
					</tr>
				
					<tr ng-repeat="team in group.group_participants | orderBy:['-points', '-plus']">
						<td><% team.name %></td>
						@if($tournament->owner())
						<td ng-repeat="opponent in group.group_participants| filter: {team : '!{{$tournament->type}}'} | orderBy:['-points', '-plus']" class="match text-center" ng-class="{self: opponent.id == team.id, hover: opponent.id != team.id}">
							<div ng-repeat="match in team.matches | filter:{opponent:opponent.id}:true" ng-click="getMatch(match.id)" data-toggle="modal" data-target="#changeScore">
								<% match.score %> : <% match.opp_score %>
							</div>
							<div ng-if='need(team.matches, "opponent", opponent.id) && opponent.id != team.id' ng-click="createMatch(team.id, opponent.id)" data-toggle="modal" data-target="#changeScore">
								-
							</div>
						</td>
						@else
						<td ng-repeat="opponent in group.group_participants | orderBy:['-points', '-plus']" class="match text-center" ng-class="{self: opponent.id == team.id}">
							<div ng-repeat="match in team.matches | filter:{opponent:opponent.id}:true">
								<% match.score %> : <% match.opp_score %>
							</div>
							<div ng-if='need(team.matches, "opponent", opponent.id) && opponent.id != team.id'>
								-
							</div>
						</td>
						@endif
						<td class="text-center"><%team.points%></td>
						<td class="text-center"><%team.plus%></td>
						<td class="text-center"><%$index+1%></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="changeScore">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="h4" ng-if="match.id">Mainīt rezultātu</h4>
					<h4 class="h4" ng-if="!match.id">Pievienot rezultātu</h4>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<form name="scoreform">
							<div class="col-md-6">
								<label for="home"><% match.home.name %></label>
								<input type="text" class="form-control" id="home" ng-model="match.home_score" value="<% match.home_score %>" ng-pattern="/^[0-9]{1,7}$/" required>
							</div>
							<div class="col-md-6">
								<label for="away"><% match.away.name %></label>
								<input type="text" class="form-control" id="away" ng-model="match.away_score" value="<% match.away_score %>" ng-pattern="/^[0-9]{1,7}$/" required>
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" ng-if="match.id" ng-click="deleteMatch()" data-dismiss="modal">Dzēst</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveMatch()" ng-disabled="scoreform.$invalid">Saglabāt</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection