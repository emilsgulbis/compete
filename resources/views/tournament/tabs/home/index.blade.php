@extends('tournament.single')

@section('tabs')

<div class="row">
	<div class="col-md-7">
		<h3>Apraksts</h3>
		<p>{!!nl2br($tournament->description)!!}</p>
	</div>
	<div class="col-md-5">
		<h3>Statistika</h3>
		<ul class="list-group">
			<li class="list-group-item">
				<p class="h4">{{$tournament->participants->count()}} <span class="text-muted h5">dalībnieks/-i</span></p>
			</li>
			<li class="list-group-item">
				<p class="h4">{{$tournament->groups()->count()}} <span class="text-muted h5">grupa/-s</span></p>
			</li>
			<li class="list-group-item">
				<p class="h4">{{$tournament->playoffs->count()}} <span class="text-muted h5">izslēgšanas spēļu koki</span></p>
			</li>
			<li class="list-group-item">
				<p class="h4">{{$tournament->events()->count()}} <span class="text-muted h5">reģistrēti notikums/-i</span></p>
			</li>
			<li class="list-group-item">
				<p class="h4">{{$tournament->posts->count()}} <span class="text-muted h5">jaunums/-i</span></p>
			</li>
		</ul>

		<h3>Par turnīru</h3>
		<ul class="list-group">
			<li class="list-group-item">
				<span class="text-muted">Organizators</span>
				<p class="h4">{{$tournament->user->name}}</p>
			</li>
			<li class="list-group-item">
				<span class="text-muted">Sporta veids</span>
				<p class="h4">{{$tournament->sport->name}}</p>
			</li>
		</ul>
	</div>
</div>

@endsection