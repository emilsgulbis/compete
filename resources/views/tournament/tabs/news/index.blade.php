@extends('tournament.single')

@section('tabs')

@if($tournament->owner())
	<a href="{{url('t', $tournament->slug)}}/news/new" class="btn btn-primary m-b">Izveidot jaunu</a>
@endif

<div ng-controller="postsController" ng-init="init({{$tournament->id}})">
	<div class="post" ng-repeat="post in posts" ng-if="!loading">
		<div class="panel">

			<img ng-if="post.image" ng-src="{{asset('upload/news/<%post.image%>.jpg')}}" alt="<% post.title %>" class="img-full">
			<div class="wrapper">
				<h2 class="m-t-none text-black"><% post.title %></h2>
				<div><% post.text %></div>

				<div class="line line-lg b-b b-light"></div>
				<div class="text-muted">
					<i class="fa fa-user"></i>
					<span class="m-r"> <% post.author.name %></span>
					<i class="fa fa-clock-o"></i>
					<span class="m-r"> <% post.created_at %></span>
					@if($tournament->owner())
						<a href="#" ng-click="deletePost($index)" class="text-muted pull-right">
							<i class="fa fa-times"></i>
							<span>Dzēst</span>
						</a>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="alert alert-info" ng-if="posts.length == 0">
		Šim turnīram nav jaunumu
	</div>

	<div class="text-center m-t-lg m-b-lg" ng-if="loading || more_loading">
		<i class="fa fa-3x fa-spin fa-circle-o-notch"></i>
	</div>
	
	<div class="text-center" ng-show="load_more" ng-if="!loading || !more_loading">
		<button class="btn btn-success" ng-click="loadMore()">Ielādēt vēl</button>
	</div>
</div>

@endsection