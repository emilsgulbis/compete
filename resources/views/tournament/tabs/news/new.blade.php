@extends('tournament.single')

@section('tabs')

<div ng-controller="postsController">

{!! Form::open(array('route'=>'new-post', 'files'=>true)) !!}
	
	{!! Form::hidden('tournament_id', $tournament->id) !!}

	<div class="thumbnail" ng-if="image[0]">
		<img ngf-src="image[0]" alt="">
	</div>

    <input type="file" ngf-select="" ng-model="image" name="image" accept="image/*" class="hidden" id="uploadImage">
	
	<div class="form-group">
      	<a href="#" class="btn btn-default btn-addon" ng-click="clickUploadImage()"><i class="icon icon-picture"></i> Pievienot bildi</a>
	</div>

	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label for="name" id="name" class="control-label">Virsraksts <span class='text-danger'>*</span></label>
	{!! Form::text('name', old('name'), ['class'=>'form-control']) !!}
	</div>
	
	<div class="form-group">
	{!! Form::label('text', 'Teksts') !!}
	{!! Form::textarea('text', old('text'), ['class'=>'form-control']) !!}
	</div>
	<p><span class="text-danger">*</span> <em>Obligāti aizpildāmie lauki</em></p>

	{!! Form::submit('Pievienot', ['class'=>'btn btn-success']) !!}
{!! Form::close() !!}

</div>

@endsection