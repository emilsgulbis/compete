@extends('tournament.single')

@section('tabs')

<div ng-controller="playoffController" ng-init="init({{$tournament->id}})">

	<div class="text-center m-t-lg" ng-if="loading">
		<i class="fa fa-3x fa-spin fa-circle-o-notch"></i>
	</div>
	
	<div ng-if="!loading">
		@if($tournament->owner())
		<form class="m-b" name="playoffForm">
			<h4>Jauns izslēgšanas spēļu koks</h4>
			<div class="form-group">
				<label for="playoff-name">Nosaukums <span class="text-danger">*</span></label>
				<input type="text" class="form-control" id="playoff-name" ng-model="playoff.name" required>
			</div>
			<div class="form-group">
				<label for="playoff-level">Sākuma kārta <span class="text-danger">*</span></label>
				<select class="form-control" ng-model="playoff.level" id="playoff-level" required>
					<option value=""></option>
					<option value="4">1/8 (16 komandas)</option>
					<option value="3">1/4 (8 komandas)</option>
					<option value="2">1/2 (4 komandas)</option>
					<option value="1">Fināls (2 komandas)</option>
				</select>
			</div>

			<p><span class="text-danger">*</span> <em>Obligāti aizpildāmie lauki</em></p>

			<div class="form-group">
				<button class="btn btn-success" ng-click="addPlayoff()" ng-disabled="playoffForm.$invalid">Pievienot</button>
			</div>
			
			<div class="line line-dashed b-b line-lg b-gray"></div>
		</form>
		@endif

		<div class="panel panel-default" ng-repeat="playoff in playoffs">
			<div class="panel-heading text-center pos-rlt">
				@if($tournament->owner())
					<a ng-click="removePlayoffAlert($index)" class="right-xs m-r-xs pos-abt text-muted">
						<i ng-show="!loading_remove" class="fa fa-times"></i>
						<i ng-show="loading_remove" class="fa fa-spin fa-circle-o-notch"></i>
					</a>
	            	<a editable-text="playoff.name" onbeforesave="updatePlayoff($data, playoff.id)"><% playoff.name || 'ievadiet nosaukumu' %></a>

	            @else
					<% playoff.name %>
	            @endif
			</div>

			<i class="fa fa-spin fa-refresh" ng-if="loading"></i>
			
			<div class="bracket hbox">
				<div ng-repeat="level in playoff.levels" class="bracket-round bracket-level-<%level.size%> col">
					@if($tournament->owner())
					<div ng-repeat="group in level.groups" data-toggle="modal" data-target="#editGame" ng-click="editGame(group.games)" class="bracket-group hbox">
					@else
					<div ng-repeat="group in level.groups" class="bracket-group hbox">
					@endif
						<div class="btn-group-vertical bracket-game col v-middle">
							<div ng-repeat="game in group.games" class="bracket-team bracket-team-<%game.participant_id%> btn btn-default btn-sm btn-addon" data-team="<%game.participant_id%>">
								<% game.name %>
								<span class="pull-right"><%game.score %></span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	@if($tournament->owner())
	<!-- Modal -->
	<div class="modal fade" id="editGame">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="h4">Labot pāri</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div ng-repeat="game in games" class="col-md-6">
							<div class="form-group">
								<label for="position-<%game.position%>"><%game.position%>. pozīcija</label>
								<select class="form-control" ng-model="game.participant_id" ng-selected="game.participant_id" ng-options="team.id as team.name for team in teams">
									@if($tournament->playerType())
										<option value="">Spēlētājs</option>
									@else
										<option value="">Komanda</option>
									@endif
								</select>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" ng-model="game.score" value="<% game.score %>">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveGame()">Saglabāt</button>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>

@endsection