@extends('app')

@section('content')

<div class="col">
	<div class="bg-light lter b-b wrapper-md">
		<h1 class="m-n font-thin h3">Visi turnīri</h1>
	</div>
	<div class="wrapper">
		@if($tournaments->count())
			<div class="row">
				@foreach($tournaments as $tournament)
				<div class="col-xs-4 col-sm-3 col-md-3">
					@include('tournament.item', $tournament)
				</div>
				@endforeach
			</div>
		@else
			<p class="alert alert-info">Šobrīd nav aktīvu turnīru, bet Tu vari būt pirmais!</p>
			<a href="{{url('new')}}" class="btn btn-primary">Izveidot turnīru</a>
		@endif
	</div>
</div>

@endsection
