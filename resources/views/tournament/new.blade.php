@extends('app')

@section('content')
<div class="col">
	<div class="bg-light lter b-b wrapper-md ng-scope">
		<h1 class="m-n font-thin h3">Jauns turnīrs</h1>
	</div>

	<div class="wrapper-md">
	 	{!! Form::open() !!}
	 		<div class="form-group">
	 			{!! Form::text('name', '', ['class'=>'form-control input-lg', 'placeholder'=>'Turnīra nosaukums']) !!}
	 		</div>
	  		<div class="form-group">
	  			{!! Form::submit('Izveidot', ['class'=>'btn btn-primary'])!!}
	  		</div>
	  		
	  	{!! Form::close() !!}
	</div>
</div>
@endsection