@extends('app')

@section('content')

@if($tournament)
    <div ng-controller="settingsController" ng-init="init({{$tournament->id}})">
    
    @if($tournament->background)
        @if($tournament->owner())
            <div id="poster" class="pos-rlt" ngf-background-image="poster[0]" style="background-image:url({{asset($tournament->background_url)}})">
        @else
            <div id="poster" class="pos-rlt" style="background-image:url({{asset($tournament->background_url)}})">
        @endif
    @else
        @if($tournament->owner())
            <div id="poster" class="pos-rlt" ngf-background-image="poster[0]">
        @else
            <div id="poster" class="pos-rlt">
        @endif
    @endif

        <div class="wrapper-lg bg-black-opacity overlay">
            <div class="hbox">
                @if($tournament->logo)
                    <div class="col w-sm v-middle">
                        <img src="{{asset($tournament->logo_url)}}" ng-show="logo[0] == null" alt="" class="img-circle thumb-xl">
                        <div ng-show="logo[0] != null" class="thumb-xl image-placeholder img-circle" ngf-background-image="logo[0]"></div>
                    </div>
                @else
                    @if($tournament->owner())
                        <div class="col w-sm v-middle" ng-show="logo[0] != null" >
                            <div class="thumb-xl image-placeholder img-circle" ngf-background-image="logo[0]"></div>
                        </div>
                    @endif
                @endif

                <div class="col v-middle">
                    <h1 class="h2 text-white">{{$tournament->name}}</h1>
                </div>
            </div>
        </div>
    </div>

    <% upload.flow.files.length %>

    <div class="wrapper bg-white b-b">
        <ul class="nav nav-pills nav-sm">
            {{-- {{$active}} --}}
            <li class="active"><a href="{{url('t', $tournament->slug)}}"><i class="icon icon-home"></i></a></li>

            @if($tournament->posts->count() || $tournament->owner())
                <li><a href="{{url('t', $tournament->slug)}}/news">Jaunumi</a></li>
            @endif
            <li><a href="{{url('t', $tournament->slug)}}/calendar">Kalendārs</a></li>
            
            @if($tournament->participants->count() || $tournament->owner())
            <li><a href="{{url('t', $tournament->slug)}}/teams">{{($tournament->playerType() ? "Spēlētāji" : "Komandas")}}</a></li>
            @endif

            @if($tournament->groups()->count() || $tournament->owner())
            <li><a href="{{url('t', $tournament->slug)}}/groups">Grupas</a></li>
            @endif
            @if($tournament->playoffs->count() || $tournament->owner())
            <li><a href="{{url('t', $tournament->slug)}}/playoff">Izslēgšanas spēles</a></li>
            @endif

            @if($tournament->owner())
            <li class="pull-right"><a href="{{url('t', $tournament->slug)}}/settings">Iestatījumi</a></li>
            @endif
            {{Request::is()}}
        </ul>
    </div>
    
    <div class="wrapper">
        @yield('tabs')
    </div>

    </div>
@else
<div class="col wrapper">
    <div class="alert alert-danger">
        <p>Kā šeit nokļuvi?</p>
    </div>
</div>

@endif

@endsection