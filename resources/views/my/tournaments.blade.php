@extends('app')

@section('content')


<div class="col">
	<div class="bg-light lter b-b wrapper-md">
		<h1 class="m-n font-thin h3">Tavi turnīri</h1>
	</div>
	<div class="wrapper">
		@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
		@endif

		@if($tournaments->count())
		<div class="row">
			@foreach($tournaments as $tournament)
			<div class="col-xs-4 col-sm-3 col-md-3">
				@include('tournament.item', $tournament)
			</div>
			@endforeach
		</div>
		@else
			@if(!Session::has('success'))
			<div class="alert alert-info">Neesi izveidojis nevienu turnīru!</div>
			@endif
			<div class="row">
				<div class="col-md-6">
					<div class="bg-info dk wrapper-md r">
						<a href="{{url('new')}}">
							<h4 class="h4 m-b-xs">
								<i class="icon-trophy i-lg"></i>
								Izveido savu pirmo turnīru!
							</h4>
							<span class="text-muted">Spied šeit, lai izveidotu turnīru</span>
						</a>
					</div>
				</div>
			</div>
		@endif
	</div>
</div>

@endsection
