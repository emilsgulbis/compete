<header id="header" class="app-header navbar" role="menu">
    <div class="navbar-header bg-black box-shadow">
        <button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse">
            <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle="off-screen" target=".app-aside" ui-scroll="app">
            <i class="glyphicon glyphicon-align-justify"></i>
        </button>

        <!-- Brand -->
        <a href="{{url('/')}}" class="navbar-brand text-lt">
            <span class="hidden-folded m-l-xs">compete.lv</span>
        </a>
        <!-- / Brand -->
    </div>
</header>