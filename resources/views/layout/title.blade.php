<div class="bg-light lter b-b wrapper-md">
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<h1 class="m-n font-thin h3 text-black">{{$title}}</h1>
			@if(isset($subtitle)) <small class="text-muted">{{$subtitle}}</small> @endif
		</div>
	</div>
</div>