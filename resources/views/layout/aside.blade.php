{{-- aside --}}
<aside id="aside" class="app-aside hidden-xs bg-black">
    <div class="navbar-header bg-black box-shadow">
        <button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse">
            <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle="off-screen" target=".app-aside" ui-scroll="app">
            <i class="glyphicon glyphicon-align-justify"></i>
        </button>

        <!-- Brand -->
        <a href="{{url('/')}}" class="navbar-brand text-lt w-full">
            <span class="hidden-folded">compete.lv</span>
        </a>
        <!-- / Brand -->
        <div class="line dk m-t-none"></div>
    </div>

    <div class="aside-wrap">
        <div class="navi-wrap">
            <nav ui-nav class="navi clearfix">
                <ul class="nav">
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                        <span>Izvēlne</span>
                    </li>
                    <li>
                        <a href="{{url('/')}}">
                            <i class="icon-home icon"></i>
                            <span class="font-bold">Sākums</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('t')}}">
                            <i class="icon-trophy icon text-info-lter"></i>
                            <span class="font-bold">Visi turnīri</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-camcorder icon text-danger-lt"></i>
                            <span class="font-bold">Spēles</span>
                        </a>
                    </li>

                    <li class="line dk"></li>

                    @if(Auth::user())
                        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                            <span>Tava informācija</span>
                        </li>
                        <li>
                            <a href="{{url('my', 'tournaments')}}">
                                @if(Auth::user()->tournaments->count())
                                <b class="badge bg-info pull-right">{{Auth::user()->tournaments->count()}}</b>
                                @endif
                                <i class="icon-calendar icon text-info-lter"></i>
                                <span class="font-bold">Tavi turnīri</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('new')}}">
                                <i class="icon-plus icon text-warning"></i>
                                <span class="font-bold">Jauns turnīrs</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('auth/logout')}}">
                                <i class="icon-logout icon"></i>
                                <span class="font-bold">Izlogoties</span>
                            </a>
                        </li>
                    @else

                    @endif
                </ul>
            </nav>
        </div>
    </div>
</aside>  
{{-- aside --}}