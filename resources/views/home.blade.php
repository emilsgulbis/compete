@extends('app')

@section('content')

<div class="col">
	<div class="bg-light lter b-b wrapper-md">
		<div class="row">
			<div class="col-md-7">
				<h4 class="m-n font-thin h3">Jaunākie turnīri</h4>
			</div>
			<div class="col-md-5">
				<h4 class="m-n font-thin h3">Aktuālākie notikumi</h4>
			</div>
		</div>
	</div>
	
	<div class="wrapper">
		<div class="row">
			<div class="col-md-7">

				@if($tournaments->count())
					<div class="row">
						@foreach($tournaments as $tournament)
						<div class="col-xs-6 col-sm-4 col-md-4">
							@include('tournament.item', $tournament)
						</div>
						@endforeach
					</div>
				@else
					<p class="alert alert-info">Šobrīd nav aktīvu turnīru, bet Tu vari būt pirmais!</p>
					<a href="{{url('new')}}" class="btn btn-primary">Izveidot turnīru</a>
				@endif
			</div>
			<div class="col-md-5">
				<div class="list-group list-group-lg list-group-sp">
					@if($events->count())
					@foreach($events as $event)
					<div class="list-group-item pos-rlt">
						@if($event->live)
						<a href="{{url('t/'.$tournament->slug.'/calendar')}}/{{$event->id}}" class="label bg-danger pos-abt right">TIEŠRAIDE</a>
						@endif
						<span class="clear">
							@if($event->name)
							<span class="block font-bold">{{$event->name}}</span>
							@endif
							@if($event->home_team)
							<span class="block">{{$event->home_team->name}} - {{$event->away_team->name}}</span>
							@endif
							@if($event->place || $event->time)
							<div class="text-muted">
	                            <div class="line b-b line-lg m-b-sm m-t-sm"></div>
	                            @if($event->time)
	                            <i class="fa fa-clock-o"></i>
	                            <span class="m-r-sm">{{$event->time}}</span>
	                            @endif
	                            @if($event->place)
	                            <i class="fa fa-map-marker"></i>
	                            <span>{{$event->place}}</span>
	                            @endif
	                        </div>
	                        @endif
						</span>
						<span class="clearfix"></span>
					</div>
					@endforeach
					@endif
					
				</div>
			</div>
		</div>
		
		@if(Auth::guest())
		<div class="row">
			<div class="col-md-6">
				<div class="bg-primary dk wrapper-md r">
					<a href="{{url('auth/login')}}">
						<h4 class="h4 m-b-xs">
							<i class="icon-user-follow i-lg"></i>
							Ielogojies vai reģistrējies
						</h4>
						<span class="text-muted">Un izveido savu pirmo turnīru!</span>
					</a>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>


@endsection
