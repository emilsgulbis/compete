@extends('app')

@section('content')

<div class="bg-light lter b-b wrapper-md">
	<h5 class="m-n font-thin h3">Ielogojies</h5>
</div>

<div class="wrapper-md">
	<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-body">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Ups!</strong> Pārbaudi datus un mēģini vēlreiz!.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			
				{!! Form::open() !!}
					
					<div class="form-group">
						{!! Form::label('email', 'E-pasts', ['class'=>'control-label']) !!}
						{!! Form::email('email', old('email'), ['class'=>'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('password', 'Parole', ['class'=>'control-label']) !!}
						{!! Form::password('password', ['class'=>'form-control']) !!}
					</div>

					<div class="form-group">
					{!! Form::submit('Ienākt', ['class'=>'btn btn-info pull-right']) !!}
						<div class="checkbox pull-left">
							<label class="i-checks">
								{!! Form::checkbox('remember') !!}
								<i></i>
								Atcerēties mani
							</label>
						</div>

					</div>
				
				{!! Form::close() !!}

			</div>
		</div>

		<a class="btn btn-link" href="{{ url('/password/email') }}">Aizmirsi paroli?</a>

		<a href="{{url('auth/register')}}" class="btn btn-success pull-right">Reģistrējies</a>
	</div>

	</div>
</div>

@endsection
