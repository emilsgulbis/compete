<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Compete</title>
	
	{!! Html::style('css/bootstrap.css') !!}
	{!! Html::style('css/template.css') !!}
	{!! Html::style('css/app.css') !!}
	{!! Html::style('//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css') !!}
	{!! Html::style('css/simple-line-icons.css') !!}
	{!! Html::style('css/xeditable.css') !!}
	{!! Html::style('css/sweetalert.css')!!}
	{!! Html::style('//cdnjs.cloudflare.com/ajax/libs/animate.css/3.3.0/animate.min.css')!!}

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body ng-app="competeApp">
	
	<div class="app app-aside-fixed container">
		{{-- @include ('layout/header') --}}
		@include ('layout/aside')

		<!-- Content -->
		<div id="content" class="app-content" role="main">
			<div class="app-content-body">
				<div class="hbox hbox-auto-xs hbox-auto-sm">
					@yield('content')
				</div>
			</div>
		</div>

		@include ('layout/footer')
	</div>
	
	{{-- Scripts --}}
	{!! Html::script("//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js") !!}
	{!! Html::script("https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js") !!}
	{!! Html::script("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js") !!}
	{!! Html::script("//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.0/ui-bootstrap-tpls.min.js") !!}

	{{-- Angular helpers --}}
	{!! Html::script("js/xeditable.js") !!}
	{!! Html::script("js/Chart.min.js") !!}
	{!! Html::script("js/angular-chart.js") !!}
	{!! Html::script("js/ng-file-upload-all.min.js") !!}
	{!! Html::script("js/sweetalert.min.js") !!}
	{!! Html::script('js/app.js') !!}
	
	<script>angular.module("competeApp").constant("CSRF_TOKEN", '{{ csrf_token() }}');</script>
	@yield('scripts')
</body>
</html>
