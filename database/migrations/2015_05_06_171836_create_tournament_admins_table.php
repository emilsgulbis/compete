<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');

			$table->tinyInteger('role')->default(0); // 0 - Referee // 1 - organizer
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_admins');
	}

}
