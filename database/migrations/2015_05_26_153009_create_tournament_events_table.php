<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('home_team_id')->unsigned()->nullable();
			$table->foreign('home_team_id')->references('id')->on('tournament_participants');

			$table->integer('away_team_id')->unsigned()->nullable();
			$table->foreign('away_team_id')->references('id')->on('tournament_participants');

			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments');

			$table->string('name', 100);
			$table->date('date');
			$table->time('time');
			$table->string('place', 100);
			$table->boolean('live');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_events');
	}

}
