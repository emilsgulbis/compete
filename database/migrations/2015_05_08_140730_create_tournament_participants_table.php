<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentParticipantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_participants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments');

			$table->integer('parent_id')->unsigned()->nullable();
			$table->foreign('parent_id')->references('id')->on('tournament_participants');

			$table->string('name',100);
			$table->date('date');
			$table->string('place',100);

			$table->string('image');
			$table->string('logo');

			$table->tinyInteger('type')->default(0); // 0 - teams, 1 - individual, 2 - king of the

			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_participants');
	}

}
