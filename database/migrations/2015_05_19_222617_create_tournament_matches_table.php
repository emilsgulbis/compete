<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentMatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_matches', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('gp_home_id')->unsigned();
			$table->foreign('gp_home_id')->references('id')->on('group_participants');

			$table->integer('gp_away_id')->unsigned();
			$table->foreign('gp_away_id')->references('id')->on('group_participants');

			$table->integer('home_score');
			$table->integer('away_score');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_matches');
	}

}
