<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupParticpantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_participants', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('group_id')->unsigned();
			$table->foreign('group_id')->references('id')->on('tournament_groups');

			$table->integer('participant_id')->unsigned();
			$table->foreign('participant_id')->references('id')->on('tournament_participants');
			$table->softDeletes();
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_participants');
	}

}
