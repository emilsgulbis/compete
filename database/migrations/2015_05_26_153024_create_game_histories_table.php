<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('tournament_events');

			$table->string('message')->nullable();
			$table->string('key');
			$table->integer('value')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_history');
	}

}
