<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentPlayoffsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_playoffs', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->integer('participant_id')->unsigned();
			$table->foreign('participant_id')->references('id')->on('tournament_participants');

			$table->integer('playoff_id')->unsigned();
			$table->foreign('playoff_id')->references('id')->on('tournament_groups');

			$table->tinyInteger('position');
			$table->tinyInteger('level');
			$table->integer('score')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_playoffs');
	}

}
