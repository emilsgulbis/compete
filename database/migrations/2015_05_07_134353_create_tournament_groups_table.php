<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('tournament_id')->unsigned();
			$table->foreign('tournament_id')->references('id')->on('tournaments');

			$table->string('name');
			$table->boolean('playoff');

			$table->tinyInteger('type')->default(0);// 0 - teams, 1 - individual, 2 - king of the
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_groups');
	}

}
