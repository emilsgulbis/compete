<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			
			$table->string('name', 100);
			$table->string('slug', 100);
			$table->text('description');
			$table->string('logo');
			$table->string('background');

			$table->boolean('visible')->default(0);

			// FROM SETTINGS
			$table->integer('sport_id')->unsigned()->nullable();
			$table->foreign('sport_id')->references('id')->on('sports');

			$table->tinyInteger('type')->default(0); // 0 - teams, 1 - individual, 2 - king of the

			$table->boolean('groups')->default(false);
			$table->boolean('playoff')->default(false);
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournaments');
	}

}
