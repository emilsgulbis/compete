<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');

Route::group(['middleware'=>'auth'], function(){
	Route::get('new', 'TournamentController@getCreate');
	Route::post('new', 'TournamentController@postCreate');
});
Route::get('t/{slug?}/{tabs?}/{action?}',['uses'=>'TournamentController@getTab', 'as'=>'get-tab']);

// Post tournament tabs
Route::post('save-settings', ['uses'=>'TournamentController@saveSettings', 'as'=>'save-settings']);
Route::post('new-post', ['uses'=>'TournamentController@newPost', 'as'=>'new-post']);
Route::post('add-participant', ['uses'=>'TournamentController@addParticipant', 'as'=>'add-participant']);
Route::post('change-live', ['uses'=>'EventController@changeLive', 'as'=>'change-live']);

Route::controller('my', 'UserController');

Route::group(['prefix'=>'api'], function(){
	Route::get('test', 'EventController@test');

	Route::get('tournament/slug', 'TournamentController@getSlug');
	Route::get('tournament/slug/validate', 'TournamentController@validateSlug');

	Route::post('teams/remove', 'TeamsController@remove');
	Route::resource('teams', 'TeamsController', ['only'=>['store', 'show']]);
	Route::resource('posts', 'PostsController');

	Route::controller('group', 'GroupsController');

	Route::controller('playoff', 'PlayoffController');

	Route::controller('match', 'MatchesController');

	Route::controller('event', 'EventController');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
