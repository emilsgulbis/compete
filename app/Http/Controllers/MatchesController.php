<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Tournament;
use App\TournamentMatch;
use App\GroupParticipant;
use App\TournamentGroup;

use Request;
use Validator;

class MatchesController extends Controller {

	public function getIndex(){
		if(Request::has('id'))
			return TournamentMatch::with(['home', 'away'])->find(Request::get('id'));
		return 'Match ID not set';
	}

	public function postSave(){
		$data = Request::get('match');

		if(Request::has('match.id')){
			$match = TournamentMatch::find($data['id']);
			$match->home_score = $data['home_score'];
			$match->away_score = $data['away_score'];
		}
		else{
			$match = TournamentMatch::create($data);
		}
		$match->save();

		$tournament = GroupParticipant::find($data['gp_home_id'])->group->tournament;
		
		return Tournament::find($tournament->id)->groups()->select(['id', 'tournament_id', 'name'])->with('groupParticipants')->get();
	}

	public function postCreate(){
		$home_id = Request::get('home_id');
		$away_id = Request::get('away_id');

		$home = GroupParticipant::find($home_id);
		$away = GroupParticipant::find($away_id);

		$match = TournamentMatch::firstOrNew(['gp_home_id' => $home_id, 'gp_away_id'=>$away_id]);
		$match->home = $home;
		$match->away = $away;

		return $match;
	}

	public function postDelete(){
		$data = Request::get('match');
		$match = TournamentMatch::find($data['id']);
		$match->delete();

		$tournament = GroupParticipant::find($data['gp_home_id'])->group->tournament;
		return Tournament::find($tournament->id)->groups()->select(['id', 'tournament_id', 'name'])->with('groupParticipants')->get();
	}
}