<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Tournament;
use App\TournamentGroup;
use App\GroupParticipant;

use Request;
use Validator;

class GroupsController extends Controller {

	public function getIndex($id = null){
		if(Request::has('tournament_id'))
			return $this->show(Request::get('tournament_id'));
		return 'Tournament ID not set';
	}

	public function show($id = null){
		if($id)
			return Tournament::find($id)->groups()->select(['id', 'tournament_id', 'name'])->with('groupParticipants')->get();
		return 'Tournament ID not set';
	}

	public function postIndex(){
		$data = Request::all();
		$rules = array(
			'name' => 'required|min:2'
		);
		$validator = Validator::make($data, $rules);
		if($validator->passes()){
			$tournament = Tournament::find($data['tournament_id']);
			$group = new TournamentGroup;
			$group->name = $data['name'];
			$group->type = $tournament->type;
			$group->playoff = 0;
			$group->tournament_id = $tournament->id;
			$group->save();
			return $group;
		}
		return false;
	}

	public function getParticipants(){
		$tournament = Tournament::find(Request::get('tournament_id'));
		return $tournament->participants;
	}

	public function postParticipant(){
		$data = Request::all();
		$group = TournamentGroup::find($data['group_id']);
		$group->participants()->sync([$data['team_id']], false);

		$tournament = $group->tournament;
		return $this->show($tournament->id);
	}

	public function postUpdate(){
		$data = Request::all();
		$group = TournamentGroup::find($data['id']);
		if($data['name']){
			$group->name = $data['name'];
			$group->save();
		}
	}

	public function postRemove(){
		$data = Request::all();
		$group = TournamentGroup::find($data['id']);
		$group->delete();
	}
}
