<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Tournament;
use App\TournamentEvent;
use App\EventHistory;

use Request;
use Validator;
use DB;
use Response;

use Carbon\Carbon;

class EventController extends Controller {
	public function getIndex($id = null){
		if(Request::has('tournament_id'))
			return $this->show(Request::get('tournament_id'));
		return 'Tournament ID not set';
	}

	public function show($id = null){
		if($id){
			$events = Tournament::find($id)
				->events()
				->orderBy('date')
				->groupBy('date')
				->select(['date'])
				->with(['games'=>function($game){
					$game->with(['home_team', 'away_team']);
				}])
				->get();

			return $events;
		}
		return 'Tournament ID not set';
	}

	public function postIndex(){
		
		$event = Request::get('event');
		TournamentEvent::create($event);
		return $this->show(Request::get('tournament_id'));
	}

	public function postRemove(){
		$event_id = Request::get('event_id');
		TournamentEvent::find($event_id)->delete();
	}

	public function getHistory(){
		if(!Request::has('event_id'))
			return 'Event ID not set';
		$event_id = Request::get('event_id');

		return $this->history($event_id);
	}

	public function changeLive(){
		$event = TournamentEvent::find(Request::get('event_id'));
		$event->live = Request::get('live');
		$event->save();
		return redirect()->back();
	}

	public function history($event_id){
		$event = TournamentEvent::with(['home_team', 'away_team', 'history', 'public_history'])->find($event_id);
		$statistic = $this->statistic($event);

		$response = new Blank;
		$response->event = $event;
		$response->statistic = $statistic;

		return Response::json($response);
	}

	public function test(){
		return TournamentEvent::find(3)->public_history;
	}

	public function postHistory(){
		$event_id = Request::get('event_id');
		$history = EventHistory::create(Request::all());
		$history->save();

		return $this->history($event_id);
	}

	public function postHistoryRemove(){
		$id = Request::get('id');
		$history = EventHistory::find($id);
		$event_id = $history->event_id;
		$history->delete();
		return $this->history($event_id);
	}

	public function statistic($event){
		$history = $event->history()->where('key', 'LIKE', '%_score')->get();

		$dates = $event->history()->where('key', 'LIKE', '%_score')->lists('created_at');

		$statistic = new Blank;
		$statistic->dates = $dates;
		$statistic->home = [];
		$statistic->away = [];

		$home = $away = 0;

		foreach($history as $date){
			if($date->key == 'home_score')
				$home += $date->value;
			else
				$away += $date->value;

			$statistic->home[] = $home;
			$statistic->away[] = $away;
		}

		return $statistic;
	}

	public function getStatistic(){
		if(!Request::has('event_id'))
			return 'Event ID not set';
		$event_id = Request::get('event_id');
		$event = TournamentEvent::find($event_id);
		
		return Response::json($this->statistic($event));
	}
}

Class Blank{}