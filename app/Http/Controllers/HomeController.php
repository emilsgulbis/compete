<?php namespace App\Http\Controllers;

use Carbon\Carbon;
class HomeController extends Controller {

	public function __construct(){
		$this->middleware('guest');
	}

	public function index(){
		$today = Carbon::now()->toDateString();
		$tournaments = \App\Tournament::where('visible', 1)->take(6)->get();
		$events = \App\TournamentEvent::take(6)->orderBy('date')->whereRaw('DATE(date) >= ?', [$today])->get();
		$data = array(
			'tournaments' => $tournaments,
			'events' => $events
		);
		return view('home', $data);
	}

}
