<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\TournamentPost;
use App\Tournament;

use Request as Req;
use File;

class PostsController extends Controller {

	public function index(){
		$tornament_id = Req::get('tournament_id');
		
		$items = Tournament::find($tornament_id)->posts()->with('author')->paginate(2);
		return $items;
	}

	public function destroy($id) {
		$post = TournamentPost::find($id);
		if($post->image){
			$file = 'upload/news/' . $post->image . '.jpg';
			File::delete($file);
		}
		TournamentPost::destroy($id);
	}
}
