<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller {

	public function getTournaments(){
		$tournaments = \Auth::user()->tournaments;

		$data = array(
			'tournaments' => $tournaments
		);
		return view('my.tournaments', $data);
	}

}