<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TournamentController extends Controller {

	// Create tournament
	public function getCreate(){
		return view('tournament.new');
	}

	public function getSlug(){
		$tournament = \App\Tournament::find(\Request::get('id'));
		return $tournament->slug;
	}

	public function validateSlug(){
		$slug = \Request::get('slug');
		$tournament = \App\Tournament::where('slug', $slug)->first();
		if(is_null($tournament))
			 return response()->json(true);
		return response()->json(false);
	}

	public function postCreate(){
		$data = \Input::all();

		$rules = array(
			'name' => 'required|min:3'
		);

		$validator = \Validator::make($data, $rules);
		if($validator->passes()){
			$tournament = new \App\Tournament;
			$tournament->name = $data['name'];
			$tournament->user_id = \Auth::id();
			$tournament->save();

			return redirect()->route('get-tab', [$tournament->slug, 'settings']);
		}

		return redirect()->back()->withErrors($validator->errors());
	}

	// Save settings
	public function saveSettings(){
		$data = \Input::all();

		$rules = array(
			'name' => 'required|min:3',
			'slug' => 'required|min:3'
		);

		$validator = \Validator::make($data, $rules);

		if($validator->passes()){
			$tournament = \App\Tournament::find($data['id']);

			$tournament->name = $data['name'];
			if($tournament->slug != $data['slug'])
				$tournament->slug = \App\Tournament::makeSlugUnique($data['slug']);
			$tournament->description = $data['description'];
			$tournament->visible = $data['visible'];

			$tournament->sport_id = ($data['sport_id'] == '' ? NULL : $data['sport_id']);

			$tournament->type = $data['type'];
			$tournament->groups = \Input::has('groups');
			$tournament->playoff = \Input::has('playoff');

			if(\Input::hasFile('logo')){
				$tournament->logo = sha1(time());
			    $img = \Image::make(\Input::file('logo'));
			    $img->fit(300);
			    $img->save('upload/tournaments/logos/'.$tournament->logo.'.jpg');
			}

			if(\Input::hasFile('poster')){

				$tournament->background = sha1(time());
			    $img = \Image::make(\Input::file('poster'));
			    $img->resize(970, null, function ($constraint) {
			        $constraint->aspectRatio();
			    });
			    $img->save('upload/tournaments/posters/'.$tournament->background.'.jpg');
			}

			$tournament->save();

			return redirect()->route('get-tab', [$tournament->slug, 'settings']);
		}

		return redirect()->back()->withErrors($validator->errors());
	}

	// New post
	public function newPost(){
		$data = \Input::all();

		$rules = array(
			'name' => 'required|min:3'
		);

		$validator = \Validator::make($data, $rules);

		if($validator->passes()){
			$post = new \App\TournamentPost;
			$tournament = \App\Tournament::find($data['tournament_id']);

			$post->title = $data['name'];
			$post->text = $data['text'];
			$post->tournament_id = $tournament->id;
			$post->user_id = \Auth::id();

			if(\Input::hasFile('image')){

				$post->image = sha1(time());
			    $img = \Image::make(\Input::file('image'));
			    $img->resize(940, null, function ($constraint) {
			        $constraint->aspectRatio();
			    });
			    $img->save('upload/news/'.$post->image.'.jpg');
			}

			$post->save();

			return redirect()->route('get-tab', [$tournament->slug, 'news']);
		}
		return redirect()->back()->withErrors($validator->errors());
	}

	// Add participant
	public function addParticipant(){
		$data = \Input::all();
		$rules = array(
			'name' => 'required|min:2'
		);
		$validator = \Validator::make($data, $rules);
		if($validator->passes()){
			$tournament = \App\Tournament::find($data['tournament_id']);
			if($tournament->playerType())
				$participant = new \App\TournamentPlayer;
			else
				$participant = new \App\TournamentTeam;

			$participant->name = $data['name'];
			$participant->tournament_id = $tournament->id;
			$participant->save();
		}
	}

	public function getTab($slug = null, $tabs = null, $action = null){
		// All Visible tournaments
		if(!$slug){
			$tournaments = \App\Tournament::where('visible', 1)->get();	
			$data = array('tournaments'=>$tournaments);
			return view('tournament.visible', $data);
		}
		// Tabs
		else{
			$tournament = \App\Tournament::findBySlug($slug);
			$data = array('tournament'=>$tournament);
			if($tabs == 'calendar' && $action){
				$id = $action;
				$event = \App\TournamentEvent::find($id);
				if($event)
					$tournament = $event->tournament;
				$data = array(
					'event' => $event,
					'tournament' => $tournament
				);
				return view('tournament.tabs.calendar.event', $data);
			}
			if($tabs == 'remove'){
				if($tournament->owner()){
					$tournament->delete();
					return redirect()->action('UserController@getTournaments')->with('success', 'Turnīrs "' . $tournament->name . '" veiksmīgi dzēsts!');
				}	
			}
			if(!$action) $action = 'index';
			$view = 'tournament.tabs.'.$tabs.'.'.$action;
			if(!\View::exists($view)){
				$tabs = 'home';
				$view = 'tournament.tabs.home.index';
			}
			$data['active'] = $tabs;
			return view($view, $data);
		}
	}


}
