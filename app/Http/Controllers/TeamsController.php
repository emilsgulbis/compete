<?php namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\TournamentParticipant;
use App\Tournament;
use Request;
use Validator;

class TeamsController extends Controller {

	public function show($id){
		$tournament = Tournament::find($id);
		if($tournament->playerType())
			return $tournament->players;
		return $tournament->teams;
	}

	public function store() {
		$data = Request::all();
		$rules = array(
			'name' => 'required|min:2'
		);
		$validator = Validator::make($data, $rules);
		if($validator->passes()){

			$tournament = Tournament::find($data['tournament_id']);
			$participant = new TournamentParticipant;
			$participant->name = $data['name'];
			$participant->tournament_id = $tournament->id;
			$participant->type = $tournament->type;
			$participant->save();
		}

		return $participant;
	}

	public function remove() {
		$id = Request::input('team_id');
		$tournament = Tournament::find(Request::input('tournament_id'));

		$participant = TournamentParticipant::find($id);
		
		$participant->eventsHome()->delete();
		$participant->eventsAway()->delete();

		foreach($participant->group_participants as $gp){
			$gp->matchesAway()->delete();
			$gp->matchesHome()->delete();
		}
		$participant->group_participants()->delete();

		$participant->delete();
	}
}
