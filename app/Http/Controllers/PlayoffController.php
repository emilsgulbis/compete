<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Tournament;
use App\TournamentGroup;
use App\TournamentPlayoff;
use App\GroupParticipant;

use Request;
use Validator;

class PlayoffController extends Controller {

	public function getIndex($id = null){
		if(Request::has('tournament_id'))
			return $this->show(Request::get('tournament_id'));
		return 'Tournament ID not set';
	}

	public function show($id = null){
		if($id){
			$playoffs = Tournament::find($id)
				->playoffs()
				->select(['id', 'tournament_id', 'name', 'playoff as size'])
				->get();

			foreach ($playoffs as $playoff) {
				$playoff->levels = $this->getLevels($playoff->id);
			}
			return $playoffs;
		}
		return 'Tournament ID not set';
	}

	public function postIndex(){
		$data = Request::all();
		$rules = array(
			'name' => 'required|min:2',
			'level' => 'required'
		);
		$validator = Validator::make($data, $rules);
		if($validator->passes()){
			$tournament = Tournament::find($data['tournament_id']);
			$group = new TournamentGroup;
			$group->name = $data['name'];
			$group->type = $tournament->type;
			$group->playoff = $data['level'];
			$group->tournament_id = $tournament->id;
			$group->save();
			return $this->show($data['tournament_id']);
		}
		return false;
	}
	
	public function getLevels($group_id){
		
		$main = TournamentGroup::find($group_id);

		$max_level = $main->playoff;

		$levels = [];
		for($i = $max_level; $i > 0; $i--){

			// if($i == $max_level)
			// 	$past = 0;
			// else
			// 	$past += pow(2,$i+1);
			$past = 0;

			$team_count = pow(2, $i);
			$group_count = $team_count/2;

			$level = new Level;
			$level->size = $i;
			$level->groups = [];

			for($j = 0; $j < $group_count; $j++){
				$group = new Group;
				$group->games = [];

				$position = $j+1;
				$playoff = TournamentPlayoff::firstOrNew(['position'=>$position+$past, 'level'=>$i, 'playoff_id'=>$main->id]);
			 	array_push($group->games, $playoff);

			 	$position = $group_count*2-$j;
			 	$playoff = TournamentPlayoff::firstOrNew(['position'=>$position+$past, 'level'=>$i, 'playoff_id'=>$main->id]);
			 	array_push($group->games, $playoff);

			 	array_push($level->groups, $group);
			}
			$levels[] = $level;
		}

		return $levels;
	}

	public function postUpdate(){
		$data = Request::all();
		$group = TournamentGroup::find($data['id']);
		$group->name = $data['name'];
		$group->save();
	}

	public function postGame(){
		$games = Request::input('games');

		foreach ($games as $game) {
			if(isset($game['id'])){
				if(isset($game['participant_id'])){
					$object = TournamentPlayoff::find($game['id']);
					$object->participant_id = $game['participant_id'];
					$object->score = $game['score'];
					$object->save();
				}
				else{
					$object = TournamentPlayoff::find($game['id']);
					$object->delete();
				}
			}
			elseif (isset($game['participant_id'])){
				$object = TournamentPlayoff::create($game);
				$object->save();
			}
			
		}
		return $this->show(Request::get('tournament_id'));
	}

	public function postRemove(){
		$data = Request::all();
		$group = TournamentGroup::find($data['id']);
		$group->delete();
	}
}

class Level{}
class Group{}