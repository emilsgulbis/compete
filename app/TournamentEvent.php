<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class TournamentEvent extends Model {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
	protected $fillable = ['home_team_id', 'away_team_id', 'tournament_id', 'name', 'date', 'time', 'place'];

	protected $appends = ['home_score', 'away_score'];

	public function getTimeAttribute($value){
		if($value) return Carbon::parse($value)->format('H:i');
	}

	public function games(){
		return $this->hasMany('App\TournamentEvent', 'date', 'date');
	}

	public function home_team(){
		return $this->belongsTo('App\TournamentParticipant', 'home_team_id');
	}

	public function away_team(){
		return $this->belongsTo('App\TournamentParticipant', 'away_team_id');
	}

	public function tournament(){
		return $this->belongsTo('App\Tournament');
	}

	public function history(){
		return $this->hasMany('App\EventHistory', 'event_id');
	}

	public function public_history(){
		return $this->hasMany('App\EventHistory', 'event_id')->where('message','!=', '');
	}

	public function getHomeScoreAttribute(){
		return $this->sum('home');
	}

	public function getAwayScoreAttribute(){
		return $this->sum('away');
	}

	public function sum($team, $key = 'score'){
		$where_key = $team . '_' . $key;
		return $this->history()->where('key', $where_key)->sum('value');
	}
}
