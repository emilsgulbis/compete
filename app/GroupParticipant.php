<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupParticipant extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $appends = ['matches', 'name', 'team', 'plus', 'points'];
	protected $casts = ['matches'=>'array'];
	protected $hidden = ['participant'];

	public function participant(){
		return $this->hasOne('App\TournamentParticipant','id', 'participant_id');
	}

	public function matchesHome(){
		return $this->hasMany('App\TournamentMatch', 'gp_home_id', 'id')->select(['id','gp_away_id as opponent', 'home_score as score', 'away_score as opp_score']);
	}

	public function matchesAway(){
		return $this->hasMany('App\TournamentMatch', 'gp_away_id', 'id')->select(['id','gp_home_id as opponent', 'away_score as score', 'home_score as opp_score']);
	}

	public function matchesWinned(){
		$home = $this->matchesHome()->whereRaw('home_score > away_score')->count();
	    $away = $this->matchesAway()->whereRaw('away_score > home_score')->count();

	    return $home+$away;
	}

	public function matchesTied(){
		$home =  $this->matchesHome()->whereRaw('home_score = away_score')->count();
		$away = $this->matchesAway()->whereRaw('away_score = home_score')->count();
		return $home + $away;
	}

	public function group(){
		return $this->hasOne('App\TournamentGroup', 'id', 'group_id');
	}

	public function getMatchesAttribute(){

		$home = $this->matchesHome()->get();
	    $away = $this->matchesAway()->get();

	    return $home->merge($away);
	}

	public function getNameAttribute(){
		return $this->participant->name;
	}

	public function getTeamAttribute(){
		return $this->participant->team;
	}

	public function getPlusAttribute(){
		$plus = $this->matches->sum('score');
		$minus = $this->matches->sum('opp_score');

		return $plus-$minus;
	}

	public function getPointsAttribute(){
		$win = 3 * $this->matchesWinned();
		$tie = $this->matchesTied();
		return $win + $tie;
	}

	public function setCreatedAtAttribute($value){
	    // Do nothing.
	}
}
