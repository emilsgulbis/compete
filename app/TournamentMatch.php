<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TournamentMatch extends Model {
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	public $timestamps = false;
	protected $fillable = ['gp_home_id', 'gp_away_id', 'home_score', 'away_score'];

	public function home(){
		return $this->hasOne('App\GroupParticipant', 'id', 'gp_home_id');
	}

	public function away(){
		return $this->hasOne('App\GroupParticipant', 'id', 'gp_away_id');
	}

}
