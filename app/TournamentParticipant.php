<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TournamentParticipant extends Model {
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
	public function groups(){
		return $this->belongsToMany('App\TournamentGroup', 'group_participants', 'participant_id', 'group_id');
	}

	public function group_participants(){
		return $this->hasMany('App\GroupParticipant', 'participant_id', 'id');
	}

	public function playoff_participants(){
		return $this->hasMany('App\TournamentPlayoff', 'participant_id', 'id');
	}

	public function eventsHome(){
		return $this->hasMany('App\TournamentEvent', 'home_team_id', 'id');
	}

	public function eventsAway(){
		return $this->hasMany('App\TournamentEvent', 'away_team_id', 'id');
	}

}
