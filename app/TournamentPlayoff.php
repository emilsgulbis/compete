<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentPlayoff extends Model {
	public $timestamps = false;
	protected $appends = ['name'];
	protected $hidden = ['participant'];

	protected $fillable = ['position', 'level', 'playoff_id', 'score', 'participant_id'];

	public function participant(){
		return $this->hasOne('App\TournamentParticipant','id', 'participant_id');
	}

	public function getNameAttribute(){
		if($this->participant)
			return $this->participant->name;
		return '-';
	}

}
