<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TournamentGroup extends Model {
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	public function teams(){
		return $this->belongsToMany('App\TournamentParticipant', 'group_participants', 'group_id', 'participant_id')->where('team', 1);
	}

	public function players(){
		return $this->belongsToMany('App\TournamentParticipant', 'group_participants', 'group_id', 'participant_id')->where('team', 0);
	}

	public function tournament(){
		return $this->belongsTo('App\Tournament');
	}

	public function groupParticipants(){
		return $this->hasMany('App\GroupParticipant', 'group_id');
	}

	public function participants(){
		return $this->belongsToMany('App\TournamentParticipant', 'group_participants', 'group_id', 'participant_id');
	}

	public function playoffParticipants(){
		return $this->hasMany('App\TournamentPlayoff', 'playoff_id');
	}

}
