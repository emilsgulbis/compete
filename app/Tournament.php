<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Tournament extends Model implements SluggableInterface{

	use SluggableTrait;
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $sluggable = array(
		'build_from' => 'name',
		'save_to'	 => 'slug',
		'unique'	 => true,
		'separator'  => '-',
		'use_cache'  => false,
		'include_trashed' => false,
	);

	protected $appends = ['logo_url', 'background_url'];

	public function getLogoUrlAttribute(){
		$hash = $this->logo;
		$url = 'upload/tournaments/logos/' . $hash . '.jpg';
		return $url;
	}

	public function getBackgroundUrlAttribute(){
		$hash = $this->background;
		$url = 'upload/tournaments/posters/' . $hash . '.jpg';
		return $url;
	}

	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function owner(){
		return ($this->user == \Auth::user());
	}

	public function posts(){
		return $this->hasMany('App\TournamentPost')->orderBy('created_at', 'desc');
	}

	public function playerType(){
		return !$this->type;
	}

	public function teams(){
		return $this->hasMany('App\TournamentParticipant')->where('type', 1);
	}

	public function players(){
		return $this->hasMany('App\TournamentParticipant')->where('type', 0);
	}

	public function team_groups(){
		return $this->hasMany('App\TournamentGroup')->where('type', 1)->where('playoff', 0);
	}

	public function player_groups(){
		return $this->hasMany('App\TournamentGroup')->where('type', 0)->where('playoff', 0);
	}

	public function team_playoffs(){
		return $this->hasMany('App\TournamentGroup')->where('type', 1)->where('playoff','>',0);
	}

	public function player_playoffs(){
		return $this->hasMany('App\TournamentGroup')->where('type', 0)->where('playoff','>',0);
	}

	public function sport(){
		return $this->belongsTo('App\Sport');
	}

	public function groups(){
		if($this->playerType())
			return $this->player_groups();
		return $this->team_groups();
	}

	public function playoffs(){
		if($this->playerType())
			return $this->player_playoffs();
		return $this->team_playoffs();
	}

	public function participants(){
		if($this->playerType())
			return $this->players();
		return $this->teams();
	}

	public function events(){
		return $this->hasMany('App\TournamentEvent');
	}
}
