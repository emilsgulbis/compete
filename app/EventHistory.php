<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EventHistory extends Model {

	//
	protected $table = 'event_history';
	protected $fillable = ['event_id', 'message', 'key', 'value'];

	public function getCreatedAtAttribute($value){
		$date = Carbon::createFromFormat('Y-m-d H:m:i', $value);
		return $date->format('H:m:i');
	}
}
