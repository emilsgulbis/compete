<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentPost extends Model {

	public function author(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function setUpdatedAtAttribute($value){
	    // Do nothing.
	}

}
