var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var request = require('request');

var json = {};
var events = [];

app.get('/', function(req, res){
  res.send('Hello there!');
});

function callback(error, response, body) {
  if (!error && response.statusCode == 200) {
    json = JSON.parse(body);
    console.log(json.event.id);
    io.sockets.in(json.event.id).emit('push score', json);
  }
}

io.on('connection', function(socket){

  console.log('a user connected');

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('remove item', function(json){
      console.log(json.event.id)
     io.sockets.in(json.event.id).emit('push score', json);
  });

  socket.on('start watch', function(event_id){
    socket.join(event_id);
    var get = {
      url: 'http://188.166.125.57/api/event/history',
      qs: {event_id:event_id}
    };
    request(get, callback);
  });

  socket.on('change score', function(data){ 
  
    var post = {
      url: 'http://188.166.125.57/api/event/history',
      form: data
    }

    // Post new 
    request.post(post, callback);
  });

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});