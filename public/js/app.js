"use strict";

var app = angular.module("competeApp", ["xeditable", "ui.bootstrap", "chart.js", "ngFileUpload"], function ($interpolateProvider) {
  $interpolateProvider.startSymbol("<%");
  $interpolateProvider.endSymbol("%>");
});

app.run(function (editableOptions) {
  editableOptions.theme = "bs3";
});

app.filter("range", function () {
  return function (input, total) {
    total = parseInt(total);
    for (var i = 0; i < total; i++) input.push(i);
    return input;
  };
});

app.config(["ChartJsProvider", function (ChartJsProvider) {
  // Configure all charts
  ChartJsProvider.setOptions({
    colours: ["#23b7e5", "#7266ba"] });
  // Configure all line charts
  ChartJsProvider.setOptions("Line", {
    datasetFill: false
  });
}]);

app.controller("teamController", function ($scope, $http) {

  $scope.teams = [];
  $scope.loading = false;
  $scope.loading_create = false;
  $scope.tournament_id;
  $scope.team = "";

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.loading = true;
    $http.get("/api/teams/" + tournament_id).success(function (data, status, headers, config) {
      $scope.teams = data;
      $scope.loading = false;
    });
  };

  $scope.addTeam = function () {
    $scope.loading_create = true;
    if ($scope.team != "") {
      $http.post("/api/teams", {
        name: $scope.team.name,
        tournament_id: $scope.tournament_id
      }).success(function (data, status, headers, config) {
        $scope.teams.push(data);
        $scope.team = "";
        $scope.loading_create = false;
      });
    }
  };

  $scope.removeTeam = function (index) {
    $scope.loading = true;

    var team = $scope.teams[index];

    $http.post("/api/teams/remove", {
      team_id: team.id,
      tournament_id: $scope.tournament_id
    }).success(function (data, status, headers, config) {
      $scope.teams.splice(index, 1);
      $scope.loading = false;
      swal({
        title: "Dzēsts!",
        text: "Dalībnieks veiksmīgi izdzēsts!",
        type: "success",
        timer: 1000
      });
    });
  };

  $scope.removeTeamAlert = function (index) {
    swal({
      title: "Tiešām vēlies dzēst?",
      text: "Šī darbība ir neatgriezeniska!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Jā!",
      cancelButtonText: "Nē!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        swal("Dzēšam...");
        $scope.removeTeam(index);
      } else {
        swal("Dzēšana atcelta!", "Dalībnieks netika izdzēsts.", "error");
      }
    });
  };
});

app.controller("postsController", function ($scope, $http) {

  $scope.posts = [];
  $scope.lastpage = 1;
  $scope.load_more = true;
  $scope.loading = false;
  $scope.more_loading = false;

  $scope.tournament_id;

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.loading = true;
    $scope.lastpage = true;
    $http({
      url: "/api/posts",
      method: "GET",
      params: {
        page: $scope.lastpage,
        tournament_id: tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.posts = data.data;
      if (data.current_page == data.last_page || data.total == 0) $scope.load_more = false;
      $scope.currentpage = data.current_page;
      $scope.loading = false;
    });
  };

  $scope.loadMore = function () {
    $scope.lastpage += 1;
    $scope.more_loading = true;
    $http({
      url: "/api/posts",
      method: "GET",
      params: {
        page: $scope.lastpage,
        tournament_id: $scope.tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.posts = $scope.posts.concat(data.data);
      if (data.current_page == data.last_page) $scope.load_more = false;

      $scope.more_loading = false;
    });
  };

  $scope.deletePost = function (index) {
    var post = $scope.posts[index];
    $http["delete"]("/api/posts/" + post.id).success(function () {
      $scope.posts.splice(index, 1);
    });
  };

  $scope.imageStrings = [];
  $scope.processFiles = function (files) {
    angular.forEach(files, function (flowFile, i) {
      var fileReader = new FileReader();
      fileReader.onload = function (event) {
        var uri = event.target.result;
        $scope.imageStrings[i] = uri;
        $scope.$apply();
      };
      fileReader.readAsDataURL(flowFile.file);
    });
  };

  $scope.clickUploadImage = function () {
    angular.element("#uploadImage").trigger("click");
  };
});

app.controller("groupController", function ($scope, $http, CSRF_TOKEN) {

  function needKey(arraytosearch, key, valuetosearch) {
    for (var i = 0; i < arraytosearch.length; i++) {
      if (arraytosearch[i][key] == valuetosearch) {
        return false;
      }
    }
    return true;
  }

  $scope.group = {};
  $scope.team = {};

  $scope.tournament_id;
  $scope.match;

  $scope.teams = [];
  $scope.groups = [];

  $scope.need = needKey;

  $scope.loading = false;
  $scope.loading_remove = false;

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.loading = true;
    $http.get("/api/group", {
      params: {
        tournament_id: tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.groups = data;
    });
    $http.get("/api/group/participants", {
      params: {
        tournament_id: $scope.tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.teams = data;
      $scope.loading = false;
    });
  };

  $scope.addGroup = function () {
    $scope.loading = true;
    if ($scope.group.name != "") {
      $http.post("/api/group", {
        name: $scope.group.name,
        tournament_id: $scope.tournament_id,
        _token: CSRF_TOKEN
      }).success(function (data, status, headers, config) {
        $scope.groups.push(data);
        $scope.group = {};
        $scope.loading = false;
      });
    }
  };

  $scope.addParticipant = function () {
    $http.post("/api/group/participant", {
      team_id: $scope.team.id,
      group_id: $scope.group.id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.groups = data;
    });
    $scope.team = {};
    $scope.group = {};
  };

  $scope.updateGroup = function (data, group_id) {
    return $http.post("/api/group/update", {
      id: group_id,
      name: data,
      _token: CSRF_TOKEN
    });
  };

  $scope.removeGroup = function (index) {
    var group = $scope.groups[index];
    $scope.loading_remove = true;

    $http.post("/api/group/remove", {
      id: group.id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.loading_remove = false;
      $scope.groups.splice(index, 1);
      swal({
        title: "Dzēsts!",
        text: "Grupa veiksmīgi izdzēsta!",
        type: "success",
        timer: 1000
      });
    });
  };

  $scope.removeGroupAlert = function (index) {
    swal({
      title: "Tiešām vēlies dzēst?",
      text: "Šī darbība ir neatgriezeniska!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Jā!",
      cancelButtonText: "Nē!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        swal("Dzēšam...");
        $scope.removeGroup(index);
      } else {
        swal("Dzēšana atcelta!", "Grupa netika izdzēsta.", "error");
      }
    });
  };

  $scope.getMatch = function (match_id) {
    $http.get("/api/match", {
      params: {
        id: match_id
      }
    }).success(function (data, status, headers, config) {
      $scope.match = data;
    });
  };

  $scope.saveMatch = function () {
    $http.post("/api/match/save", {
      match: $scope.match,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.match = "";
      $scope.groups = data;
    });
  };

  $scope.createMatch = function (home_id, away_id) {
    $http.post("/api/match/create", {
      home_id: home_id,
      away_id: away_id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.match = data;
    });
  };

  $scope.deleteMatch = function () {
    $http.post("/api/match/delete", {
      match: $scope.match,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.match = "";
      $scope.groups = data;
    });
  };
});

app.controller("playoffController", function ($scope, $http, CSRF_TOKEN) {

  function needKey(arraytosearch, key, valuetosearch) {
    for (var i = 0; i < arraytosearch.length; i++) {
      if (arraytosearch[i][key] == valuetosearch) {
        return false;
      }
    }
    return true;
  }

  $scope.tournament_id;

  $scope.playoff = {};

  $scope.teams = [];
  $scope.playoffs = [];
  $scope.games = [];

  $scope.need = needKey;

  $scope.loading = false;
  $scope.loading_remove = false;

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.loading = false;

    $http.get("/api/playoff", {
      params: {
        tournament_id: tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.playoffs = data;
    });

    $http.get("/api/group/participants", {
      params: {
        tournament_id: $scope.tournament_id
      }
    }).success(function (data, status, headers, config) {
      $scope.teams = data;
      $scope.loading = false;
    });
  };

  $scope.addPlayoff = function () {
    $scope.loading = true;
    if ($scope.playoff.name != "") {
      $http.post("/api/playoff", {
        name: $scope.playoff.name,
        tournament_id: $scope.tournament_id,
        level: $scope.playoff.level,
        _token: CSRF_TOKEN
      }).success(function (data, status, headers, config) {
        $scope.playoffs = data;
        $scope.playoff = {};
        $scope.loading = false;
      });
    }
  };

  $scope.removePlayoffAlert = function (index) {
    swal({
      title: "Tiešām vēlies dzēst?",
      text: "Šī darbība ir neatgriezeniska!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Jā!",
      cancelButtonText: "Nē!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        swal("Dzēšam...");
        $scope.removePlayoff(index);
      } else {
        swal("Dzēšana atcelta!", "Izslēgšanas spēļu koks netika izdzēsts.", "error");
      }
    });
  };

  $scope.removePlayoff = function (index) {
    var playoff = $scope.playoffs[index];
    $scope.loading_remove = true;

    $http.post("/api/playoff/remove", {
      id: playoff.id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.loading_remove = false;
      $scope.playoffs.splice(index, 1);
      swal({
        title: "Dzēsts!",
        text: "Izslēgšanas spēļu koks veiksmīgi izdzēsts!",
        type: "success",
        timer: 1000
      });
    });
  };

  $scope.editGame = function (games) {
    $scope.games = games;
  };

  $scope.updatePlayoff = function (data, playoff_id) {
    return $http.post("/api/playoff/update", {
      id: playoff_id,
      name: data,
      _token: CSRF_TOKEN
    });
  };

  $scope.saveGame = function () {
    $scope.loading = true;
    $http.post("/api/playoff/game", {
      games: $scope.games,
      tournament_id: $scope.tournament_id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.games = [];
      $scope.playoffs = data;
      $scope.loading = false;
    });
  };
});

$(function () {
  $(".app").on({
    mouseenter: function mouseenter(e) {
      $(".bracket-team-" + $(this).data("team")).addClass("bracket-team-hover");
    },
    mouseleave: function mouseleave(e) {
      $(".bracket-team-" + $(this).data("team")).removeClass("bracket-team-hover");
    }
  }, ".bracket-team");
});
app.controller("eventController", function ($scope, $http, CSRF_TOKEN) {

  $scope.events = [];
  $scope.loading = false;
  $scope.teams = [];
  $scope.event = {};

  $scope.tournament_id;

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.event.tournament_id = $scope.tournament_id;
    $scope.loading = true;

    $http.get("/api/event", {
      params: { tournament_id: tournament_id }
    }).success(function (data, status, headers, config) {
      $scope.events = data;
      $scope.loading = false;
    });

    $http.get("/api/group/participants", {
      params: { tournament_id: $scope.tournament_id }
    }).success(function (data, status, headers, config) {
      $scope.teams = data;
      $scope.loading = false;
    });
  };

  $scope.addEvent = function () {
    $scope.loading = true;

    $http.post("/api/event", {
      event: $scope.event,
      tournament_id: $scope.tournament_id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.event = { tournament_id: $scope.tournament_id };
      $scope.events = data;
      $scope.today();
      $scope.loading = false;
    });
  };

  $scope.removeEvent = function (parent_index, index) {
    $scope.loading = true;
    var ev = $scope.events[parent_index].games[index];

    $http.post("/api/event/remove", {
      event_id: ev.id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {
      $scope.events[parent_index].games.splice(index, 1);
      if ($scope.events[parent_index].games.length == 0) $scope.events.splice(parent_index, 1);
      $scope.loading = false;
    });
  };

  // Datepicker
  $scope.dateOpen = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.today = function () {
    $scope.event.date = new Date();
  };
  $scope.today();

  $scope.dateOptions = {
    formatYear: "yyyy",
    showWeeks: false,
    startingDay: 1,
    "class": "datepicker"
  };
});
app.controller("scoreboardController", function ($scope, $http, CSRF_TOKEN) {

  var socket = io.connect("http://188.166.125.57:3000");

  $scope.event_id;
  $scope.message = "";
  $scope.event = {};

  $scope.labels = [];
  $scope.series = [];
  $scope.data = [];

  $scope.init = function (event_id) {
    $scope.event_id = event_id;
    socket.emit("start watch", event_id);
  };

  $scope.removeHistory = function (id) {
    $http.post("/api/event/history-remove", {
      id: id,
      _token: CSRF_TOKEN
    }).success(function (data, status, headers, config) {

      socket.emit("remove item", data);
      console.log(data);

      swal({
        title: "Dzēsts!",
        text: "Notikums veiksmīgi izdzēsts!",
        type: "success",
        timer: 1000
      });
    });
  };

  $scope.removeHistoryAlert = function (index) {
    swal({
      title: "Tiešām vēlies dzēst?",
      text: "Šī darbība ir neatgriezeniska!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Jā!",
      cancelButtonText: "Nē!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        swal("Dzēšam...");
        $scope.removeHistory(index);
      } else {
        swal("Dzēšana atcelta!", "Notikums netika izdzēsts.", "error");
      }
    });
  };

  $scope.updateScore = function (key, value) {
    socket.emit("change score", {
      key: key,
      value: value,
      message: $scope.message,
      event_id: $scope.event_id,
      _token: CSRF_TOKEN
    });
  };

  $scope.addMessage = function () {
    socket.emit("change score", {
      key: "msg",
      message: $scope.message,
      event_id: $scope.event_id,
      _token: CSRF_TOKEN
    });
  };

  socket.on("push score", function (data) {
    $scope.event = data.event;
    $scope.statistic = data.statistic;

    $scope.labels = data.statistic.dates;
    $scope.series = [data.event.home_team.name, data.event.away_team.name];
    $scope.data = [data.statistic.home, data.statistic.away];

    $scope.message = "";
    $scope.$apply();
  });
});
app.controller("settingsController", function ($scope, $http, $window) {

  $scope.tournament = {};
  $scope.loading_slug = false;
  $scope.slug_valid = false;

  $scope.tournament_id;

  $scope.init = function (tournament_id) {
    $scope.tournament_id = tournament_id;
    $scope.getSlug(tournament_id);
  };

  $scope.getSlug = function (id) {
    $scope.loading_slug = true;
    $http.get("/api/tournament/slug", {
      params: {
        id: id
      }
    }).success(function (data, status, headers, config) {
      $scope.tournament.slug = data;
      $scope.tournament.start_slug = data;
      $scope.loading_slug = false;
      $scope.slug_valid = true;
    });
  };

  $scope.validateSlug = function () {
    $scope.loading_slug = true;

    if ($scope.tournament.slug != $scope.tournament.start_slug) {
      $http.get("/api/tournament/slug/validate", {
        params: {
          slug: $scope.tournament.slug
        }
      }).success(function (data, status, headers, config) {
        $scope.slug_valid = data;
      });
    } else {
      $scope.slug_valid = true;
    }
    $scope.loading_slug = false;
  };

  $scope.clickUploadLogo = function () {
    angular.element("#uploadLogo").trigger("click");
  };

  $scope.clickUploadPoster = function () {
    angular.element("#uploadPoster").trigger("click");
  };

  $scope.deleteTournament = function (slug) {
    swal({
      title: "Tiešām vēlies dzēst?",
      text: "Pēc dzēšanas savu turnīru atgūt nebūs iespējams!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Jā!",
      cancelButtonText: "Nē!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        $window.location.href = "/t/" + slug + "/remove";
      } else {
        swal("Dzēšana atcelta!", "Turnīrs netika izdzēsts.", "error");
      }
    });
  };
});
//# sourceMappingURL=app.js.map